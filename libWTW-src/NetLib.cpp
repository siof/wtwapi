/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <objbase.h>
#include <wchar.h>
#include <cpp/NetLib.h>
#include <cpp/Globals.h>
#include <wtwNetLib.h>

#pragma warning(disable: 4244)

namespace wtw 
{

	CNetworkSocket::CNetworkSocket(const wchar_t *id)
	{
		m_socketId = id;
	};

	CNetworkSocket::~CNetworkSocket()
	{
		destroy();
	}

	WTW_PTR CNetworkSocket::onNetlibEvent(WTW_PARAM wP, WTW_PARAM lP, void *p)
	{
		ASSERT(p);
		ASSERT(wP);
		CNetworkSocket *pSock = reinterpret_cast<CNetworkSocket *>(p);
		netLibEventDef *pEvent= reinterpret_cast<netLibEventDef *>(wP);

		switch(pEvent->eventID)
		{
		case WTW_NETLIB_EVENT_DESTROY:
			{
				pSock->onDestroy();
				return 0;
			}
		case WTW_NETLIB_EVENT_CONNECT:
			{
				pSock->onConnect(lP ? 1 : 0);
				return 0;
			}
		case WTW_NETLIB_EVENT_ERROR:
			{
				pSock->onError();
				return 0;
			}
		case WTW_NETLIB_EVENT_CLOSED:
		case WTW_NETLIB_EVENT_CLOSE:
			{
				pSock->onClose();
				return 0;
			}
		case WTW_NETLIB_EVENT_DATA:
			{
				netLibDataDef *pData = (netLibDataDef*)lP;
				pSock->onReceive((const char*)pData->data, pData->dataLen);
				return 0;
			}
		case WTW_NETLIB_EVENT_ACCEPT:
			{
				return 0;
			}
		case WTW_NETLIB_EVENT_SSL:
			{
				netLibSSLStatusDef *sslStatus = (netLibSSLStatusDef*)lP;
				pSock->onSSLStatus(sslStatus->sslStatus);
				return 0;
			}
		case WTW_NETLIB_EVENT_SSLOVR:
			{
				return 0;
			}
		case WTW_NETLIB_EVENT_SENDC:
			{
				pSock->onSendCompleted();
				return 0;
			}
		default:
			return 0;
		}
	};

	void CNetworkSocket::destroy()
	{
		ASSERT(g_pF);
		g_pF->fnCall(
			WTW_NETLIB_DESTROY, 
			reinterpret_cast<WTW_PARAM>(m_socketId.c_str()), 
			NULL);
	};

	int CNetworkSocket::create(int bUseSSL)
	{
		netLibCreateDef sdef;
		initStruct(sdef);

		sdef.callback		= &onNetlibEvent;
		sdef.cbData			= this;
		sdef.flags			= bUseSSL ? WTW_NETLIB_FLAG_SSL : 0;
		sdef.id				= m_socketId.c_str();

		return g_pF->fnCall(
			WTW_NETLIB_CREATE, 
			sdef, 
			NULL);
	};

	int CNetworkSocket::close()
	{
		return g_pF->fnCall(
			WTW_NETLIB_CLOSE, 
			reinterpret_cast<WTW_PARAM>(m_socketId.c_str()), NULL);
	};

	int CNetworkSocket::connect(const wchar_t *pHost, DWORD port)
	{
		return connect(pHost, port, NULL, 0, 0);
	};

	int CNetworkSocket::connect(QWORD devAddr, const GUID & classId) // bluetooth
	{
		wchar_t guid[255] = {0};

		if (0 == ::StringFromGUID2(
			classId, guid, 255))
			return 0;

		return connect(devAddr, guid);
	};

	int CNetworkSocket::connect(QWORD devAddr, const wchar_t *classId) // bluetooth
	{
		wchar_t host[255] = {0};
		swprintf(host, 255, L"%012llX,%s", devAddr & 0xFFFFFFFFFFFFLL, classId);

		netLibConnectDef c;
		initStruct(c);

		c.hostAddrFamily= NL_ADDR_FAMILY_BTH;
		c.hostAddr		= host;
		c.hostPort		= 0;
		c.id			= m_socketId.c_str();

		return g_pF->fnCall(
			WTW_NETLIB_CONNECT, 
			c, NULL);
	};

	int CNetworkSocket::connect(
		const wchar_t *pHost, 
		DWORD port, 
		const wchar_t *pProxyHost, 
		DWORD proxyPort,
		DWORD proxyType)
	{
		netLibConnectDef c;
		initStruct(c);

		c.hostAddr		= pHost;
		c.hostPort		= port;
		c.id			= m_socketId.c_str();

		if (pProxyHost && pProxyHost[0] != 0 && proxyType && proxyPort)
		{
			c.flags			|= WTW_NETLIB_FLAG_PROXY;
			c.proxyType		= proxyType;
			c.proxyAddress	= pProxyHost;
			c.proxyPort		= proxyPort;
		}

		return g_pF->fnCall(
			WTW_NETLIB_CONNECT, 
			c, NULL);
	};

	int CNetworkSocket::send(const void *pData, int dataLen)
	{
		netLibSendDef sd;
		initStruct(sd);

		sd.id			= m_socketId.c_str();
		sd.nBufferLen	= dataLen;
		sd.pBuffer		= reinterpret_cast<const char*>(pData);

		return g_pF->fnCall(
			WTW_NETLIB_SEND, 
			sd, NULL);
	}

	int CNetworkSocket::connectBySRV(const wchar_t *pSrvLoc, const wchar_t *pFallbackHost, DWORD fallbackPort)
	{
		netLibConnectServiceDef c;
		initStruct(c);

		c.c.hostAddr		= pFallbackHost;
		c.c.hostPort		= fallbackPort;
		c.c.id				= m_socketId.c_str();
		c.serviceName		= pSrvLoc;

		return g_pF->fnCall(
			WTW_NETLIB_CONNECT_SERVICE, 
			c, NULL);
	};

	int CNetworkSocket::startSSL()
	{
		return g_pF->fnCall(
			WTW_NETLIB_INITSSL, 
			reinterpret_cast<WTW_PARAM>(m_socketId.c_str()), 
			NULL);
	};

	void CNetworkSocket::onConnect(int bError)
	{
	};

	void CNetworkSocket::onDestroy()
	{
	};

	void CNetworkSocket::onClose()
	{
	};

	void CNetworkSocket::onSendCompleted()
	{
	};

	void CNetworkSocket::onError()
	{
	};

	void CNetworkSocket::onReceive(const char *pData, int pDataLen)
	{
	};

	void CNetworkSocket::onSSLStatus(int bSucceded)
	{
	};
};