/*
** Copyright (C) 2007-2014, K2T.eu
*/

#pragma warning(disable: 4005)

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/Bluetooth.h>

namespace wtw 
{

	CBluetooth::CBluetooth()
	{
		_portOpen = NULL;
		_portOpenSrv = NULL;
		_portClose = NULL;
		_portRead = NULL;
		_portWrite = NULL;
		//_portReset = NULL;
		//_portIsOpen = NULL;
		_enumDevices = NULL;

		m_hSocket = INVALID_SOCKET;

#ifdef _DEBUG
		m_hInstance = ::LoadLibrary(L"libBluetooth_d.module");
#else
		m_hInstance = ::LoadLibrary(L"libBluetooth.module");
#endif

		if (m_hInstance)
		{
			_enumDevices	= (btEnumDevices)	::GetProcAddress(m_hInstance, "libEnumDevices2");
			_portOpen		= (btPortOpen)		::GetProcAddress(m_hInstance, "libPortOpen");
			_portOpenSrv	= (btPortOpenSrv)	::GetProcAddress(m_hInstance, "libPortOpenService");
			_portClose		= (btPortClose)		::GetProcAddress(m_hInstance, "libPortClose");
			_portWrite		= (btPortWrite)		::GetProcAddress(m_hInstance, "libPortWrite");
			_portRead		= (btPortRead)		::GetProcAddress(m_hInstance, "libPortRead");
		}
	};

	CBluetooth::~CBluetooth()
	{
		if (m_hInstance)
		{
			::FreeLibrary(m_hInstance);
		}
	};

	HRESULT CBluetooth::openSerialPort(ULONGLONG devAddr)
	{
		if (!_portOpen)
			return E_NOINTERFACE;

		if (m_hSocket != INVALID_SOCKET)
			close();

		m_hSocket = (_portOpen)(devAddr);

		if (m_hSocket == INVALID_SOCKET)
			return E_FAIL;

		return S_OK;
	}

	HRESULT CBluetooth::openByClass(ULONGLONG devAddr, GUID guid, DWORD rcvTimeoutInMs)
	{
		if (!_portOpenSrv)
			return E_NOINTERFACE;

		if (m_hSocket != INVALID_SOCKET)
			close();

		m_hSocket = (_portOpenSrv)(devAddr, guid, rcvTimeoutInMs);

		if (m_hSocket == INVALID_SOCKET)
			return E_FAIL;

		return S_OK;
	}

	HRESULT CBluetooth::close()
	{
		if (m_hSocket == INVALID_SOCKET || !_portClose)
			return E_NOINTERFACE;

		(_portClose)(m_hSocket);

		m_hSocket = INVALID_SOCKET;
		return S_OK;
	}

	HRESULT CBluetooth::read(void *pBuffer, int bufferSize)
	{
		if (m_hSocket == INVALID_SOCKET || !_portRead)
			return E_NOINTERFACE;

		return (_portRead)(m_hSocket, (char*)pBuffer, bufferSize);
	}

	HRESULT CBluetooth::write(void *pBuffer, int bufferSize)
	{
		if (m_hSocket == INVALID_SOCKET || !_portWrite)
			return E_NOINTERFACE;

		return (_portWrite)(m_hSocket, (char*)pBuffer, bufferSize);
	}

	HRESULT CBluetooth::enumerate(std::deque<btInfo> &devs, int bScan)
	{
		if (!_enumDevices)
			return E_NOINTERFACE;

		return (_enumDevices)(devs, bScan);
	}
};