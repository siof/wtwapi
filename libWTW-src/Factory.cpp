/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/Factory.h>
#include <cpp/Internet.h>
#include <cpp/SettingsHelper.h>

namespace wtw
{
	CFactory::CFactory()
	{
	}

	HRESULT CFactory::createClassInstance(const GUID & guid, void **pClass)
	{
		if (IsEqualGUID(guid, WTW_CLASS_INTERNET_HTTP))
		{
			*pClass = new CInternetHttp();
			return S_OK;
		}
		else if (IsEqualGUID(guid, WTW_CLASS_SETTINGS))
		{
			*pClass = new CSettings();
			return S_OK;
		}
		else if (IsEqualGUID(guid, WTW_CLASS_SETTINGS_BINDER))
		{
			*pClass = new CSettingsBinder();
			return S_OK;
		}

		return E_NOINTERFACE;
	}
};