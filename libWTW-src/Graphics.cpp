/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <cpp/Globals.h>
#include <wchar.h>
#include <cpp/Graphics.h>

namespace wtw
{
	CGraphics::CGraphics(CCore *pF, const wchar_t * id) :
		m_pF(pF), m_graphId(id), m_cy(-1), m_cx(-1), m_gPointer(NULL)
	{
	}
	
	CGraphics::CGraphics(CCore *pF, const wtwGraphics & graph) : 
		m_pF(pF), m_graphId(graph.graphId), m_cx(graph.cx), m_cy(graph.cy), m_gPointer(graph.gPointer)
	{
	}

	CGraphics::~CGraphics()
	{
	}

	int CGraphics::getWidth()
	{
		return m_cx;
	}

	int CGraphics::getHeight()
	{
		return m_cy;
	}

	HRESULT CGraphicsFactory::loadPng(const wchar_t * graphId, HINSTANCE hInst, int resourceId, CGraphics **pGraph)
	{
		if (pGraph)
		{
			*pGraph = NULL;
		}

		wtwGraphics wg;

		wg.hInst = hInst;
		wg.resourceId = MAKEINTRESOURCE(resourceId);
		wg.graphId	= graphId;

		HRESULT ret = wtw::g_pF->fnCall(WTW_GRAPH_LOAD, wg, NULL);

		if (SUCCEEDED(ret) && pGraph)
		{
			*pGraph = new CGraphics(wtw::g_pF, wg);
			return S_OK;
		}

		return ret;
	}

	HRESULT CGraphicsFactory::loadPng(const wchar_t * graphId, const wchar_t * file, int bLoadFromDefLoc, CGraphics **pGraph)
	{
		if (pGraph)
		{
			*pGraph = NULL;
		}

		wtwGraphics wg;

		wg.filePath = file;
		wg.graphId = graphId;

		if (bLoadFromDefLoc)
		{
			wg.flags = WTW_GRAPH_FLAG_RELATIVE_DEF_PATH;
		}

		HRESULT ret = wtw::g_pF->fnCall(WTW_GRAPH_LOAD, wg, NULL);

		if (SUCCEEDED(ret) && pGraph)
		{
			*pGraph = new CGraphics(wtw::g_pF, wg);
			return S_OK;
		}

		return ret;
	}
};