/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/core.h>
#include <cpp/globals.h>
#include <cpp/Version.h>

static int g_refCnt = 0;

namespace wtw
{
	CCore *g_pF = NULL;
	CTranslation *g_pTrans = NULL;
	
	void LIBWTW_API libWtwInit(WTWFUNCTIONS *pFunc)
	{
		if (!g_refCnt)
		{
			g_pF = new CCore(pFunc);
			g_pTrans = new CTranslation();

			SystemVersion::m_osve.dwOSVersionInfoSize = sizeof(SystemVersion::m_osve);
			::GetVersionEx((OSVERSIONINFO*)&SystemVersion::m_osve);
		}

		g_refCnt++;
	}

	void LIBWTW_API libWtwTerminate()
	{
		g_refCnt--;

		if (!g_refCnt)
		{
			delete g_pF;
			g_pF = NULL;

			delete g_pTrans;
			g_pTrans = NULL;
		}

		if (g_refCnt < 0)
			g_refCnt = 0;
	}
}