// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <winsock2.h>
#include <windows.h>


/*
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
// TODO: reference additional headers your program requires here
*/
#ifdef _DEBUG
#include "..\libVLD\vld.h"
#endif

#define WTW_NO_API_ENTRY_EXPORT