/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <cpp/BaseObject.h>

void wtw::CBaseObject::release()
{
	delete this;
}