/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/TransferWindow.h>
#include <cpp/Globals.h>

namespace wtw {

	CTransferWindow::CTransferWindow()
	{
		ASSERT(g_pF);

		m_objectLock.Lock();
		m_pWnd		= NULL;
		m_pFnUpdate = NULL;
		m_oD		= NULL;
		m_objectLock.Unlock();
	};
	
	CTransferWindow::~CTransferWindow()
	{
		if (getCtrlPtr())
		{
			drop();
		}
	};

	WTW_PTR CTransferWindow::onStaticWindowCallback(WTW_PARAM wP, WTW_PARAM, void *od)
	{
		CTransferWindow *pWnd = (CTransferWindow *)od;
		return pWnd->onWindowCallback((wtwFTEvent*)wP);
	};

	WTW_PTR CTransferWindow::onWindowCallback(wtwFTEvent *pEvent)
	{
		return 0;
	}

	void CTransferWindow::initializeWindow(
		const wchar_t *dialogTitle,
		wtwContactDef *pContact,
		HINSTANCE hInst,
		const wchar_t *initialStatus)
	{
		m_objectLock.Lock();
		ASSERT (!m_pWnd);
		if (m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTInit fti;
		initStruct(fti);

		fti.dialogTitle = dialogTitle;
		fti.headerDesc	= pContact->id;
		fti.headerMain	= pContact->id;
		fti.hInst		= hInst;
		fti.statusText	= initialStatus;
		fti.pContact	= pContact;
		fti.fnCallback	= &onStaticWindowCallback;
		fti.fnCallbackData = this;
	
		m_pWnd	= (void*) g_pF->fnCall(WTW_FT_INIT, (WTW_PARAM)&fti, 0);

		m_pFnUpdate		= fti.fnUpdateWindow;
		m_oD			= fti.fnOwnerData;

		m_objectLock.Unlock();
	}

	void CTransferWindow::enableCloseButton(int bEnable)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle = m_pWnd;

		fts.flags			= FTF_UPDATE_ENABLE_CLOSE;
		fts.bEnableClose	= bEnable;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::enableAbortButton(int bEnable)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle = m_pWnd;

		fts.flags			= FTF_UPDATE_ENABLE_ABORT;
		fts.bEnableAbort	= bEnable;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setFileSize(ULONGLONG iSize, int bSetTotal)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_FILE_BYTES;
		fts.iFileBytes		= iSize;

		if (bSetTotal)
		{
			fts.flags		|= FTF_UPDATE_TOTAL_BYTES;
			fts.iTotalBytes = iSize;
		}

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setFileProgress(ULONGLONG iTransferred, int bSetTotal)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_FILE_SENT_BYTES | FTF_AUTO_PROGR_FILE;
		fts.iFileBytesSend	= iTransferred;

		if (bSetTotal)
		{
			fts.flags			|= FTF_UPDATE_TOTAL_SENT_BYTES | FTF_AUTO_PROGR_TOTAL;
			fts.iTotalBytesSend = iTransferred;
		}

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setStatusText(const wchar_t *ptr)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_STATUS_TEXT;
		fts.statusText		= ptr;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};
	
	void CTransferWindow::setFileLabel(const wchar_t *ptr)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_FILE_LABEL;
		fts.fileLabel		= ptr;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setOpenTarget(const wchar_t *text, const wchar_t *target)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_OPEN_LABEL | FTF_UPDATE_OPEN_TARGET;
		fts.fileOpenLabel	= text;
		fts.fileOpenTarget	= target;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setStatus(DWORD transferStatus)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_STATUS;
		fts.transferState	= transferStatus;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::drop()
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		g_pF->fnCall(WTW_FT_DROP, (WTW_PARAM)m_pWnd, 0);
		m_pWnd = NULL;
		m_objectLock.Unlock();
	};

	void CTransferWindow::setTotalSize(ULONGLONG iSize)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags		= FTF_UPDATE_TOTAL_BYTES;
		fts.iTotalBytes = iSize;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void CTransferWindow::setTotalProgress(ULONGLONG iTransferred)
	{
		m_objectLock.Lock();
		ASSERT (m_pWnd);
		if (!m_pWnd)
		{
			m_objectLock.Unlock();
			return;
		}

		wtwFTState fts;
		initStruct(fts);

		fts.windowHandle	= m_pWnd;

		fts.flags			= FTF_UPDATE_TOTAL_SENT_BYTES | FTF_AUTO_PROGR_TOTAL;
		fts.iTotalBytesSend = iTransferred;

		(m_pFnUpdate)((WTW_PARAM)&fts, 0, m_oD);
		m_objectLock.Unlock();
	};

	void *CTransferWindow::getCtrlPtr()
	{
		void *_r;
		m_objectLock.Lock();
		_r = m_pWnd;
		m_objectLock.Unlock();
		return _r;
	}
};