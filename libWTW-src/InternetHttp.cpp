/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Internet.h>
#include <cpp/Conv.h>
#include <curl/curl.h>

#pragma warning (disable: 4244) // possible loss of data
#pragma warning (disable: 4267) // possible loss of data (argument)

namespace wtw 
{
	HRESULT ctoh(CURLcode cc);

	const wchar_t * trim(wchar_t *ptr)
	{
		while (*ptr == L' ')
			(ptr)++;

		size_t l = wcslen(ptr);
		if (l)
		{
			while ((ptr[l-1]) == L' ')
			{
				l--;
				ptr[l] = 0;
			}
		}
		return ptr;
	}

	CInternetHttp::CInternetHttp()
	{
		setPostDataPtr(NULL, 0);
	};

	CInternetHttp::~CInternetHttp()
	{
	};

	HRESULT CInternetHttp::setPostDataPtr(void *ptr, DWORD len)
	{
		m_postDataPtr	= ptr;
		m_postDataLen	= len;

		return S_OK;
	}

	HRESULT CInternetHttp::cookieAddOrEdit(const wchar_t * name, const wchar_t * value)
	{
		for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
		{
			if (it->name == name)
			{
				it->value = value;
				return S_OK;
			}
		}

		httpPairW ck;

		ck.name		= name;
		ck.value	= value;

		m_cookies.push_back(ck);
		return S_OK;
	};

	const wchar_t * CInternetHttp::cookieGet(const wchar_t * name)
	{
		for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
		{
			if (it->name == name)
			{
				return it->value.c_str();
			}
		}

		return NULL;
	};

	HRESULT CInternetHttp::cookiesExport(wtw::CString & buffer)
	{
		if (!m_cookies.size())
			return S_FALSE;

		for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
		{
			buffer += it->name;
			buffer += L"=";
			buffer += it->value;
			buffer += L";";
		}
		
		return buffer.getLength();
	};

	HRESULT CInternetHttp::cookiesImport(wchar_t *data, int bClear)
	{
		if (bClear)
			cookieClear();

		if (!data)
			return E_INVALIDARG;

		size_t len = wcslen(data);
		if (0 == len)
			return E_INVALIDARG;

		size_t prevStart = 0, sepR = 0, skip = 0;

		if (!_wcsnicmp(data, L"Cookie:", wcslen(L"Cookie:")))
			skip += wcslen(L"Cookie:");

		for (size_t i = skip; i < len; ++i)
		{
			if (data[i] == L'=')
			{
				data[i] = 0;
				sepR = i;
			}

			if (data[i] == L';')
			{
				data[i] = 0;

				wchar_t * name = &data[prevStart];
				wchar_t * value= &data[sepR+1];

				name = const_cast<wchar_t*>(trim(name));
				value = const_cast<wchar_t*>(trim(value));

				cookieAddOrEdit(name, value);
				data[i]		= L';';
				data[sepR]	= L'='; 
				prevStart	= i+1;
			}
		}

		return S_OK;
	};

	HRESULT CInternetHttp::cookieRemove(const wchar_t * name)
	{
		for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
		{
			if (it->name == name)
			{
				m_cookies.erase(it);
				return S_OK;
			}
		}

		return S_FALSE;
	};

	HRESULT CInternetHttp::cookieClear()
	{
		m_cookies.clear();
		return S_OK;
	};

	HRESULT CInternetHttp::downloadFile(const wchar_t *fullUri, void *ptr, int type, const wchar_t *verb)
	{
		HRESULT ret = E_FAIL;
		int respCode = E_FAIL;
		int flags = 0;
		int bClose = false;

		if (NULL == fullUri || 0 == wcslen(fullUri))
		{
			return E_INVALIDARG;
		}

		if (isOpen() != S_OK)
		{
			bClose = true;
			open();
		}

		switch (type)
		{
		case 0:
			ret = makeHttpRequest(verb, fullUri, *((wtw::CFile*)ptr), NULL, flags, 0, 0, 0, &respCode);
			break;
		case 1:
			ret = makeHttpRequest(verb, fullUri, *((wtw::CBuffer*)ptr), NULL, flags, 0, 0, 0, &respCode);
			break;
		}

		if (bClose)
		{
			close();
		}

		if (FAILED(ret))
		{
			return ret;
		}

		return respCode;
	}

	HRESULT CInternetHttp::downloadFile(const wchar_t *fullUri, const wchar_t *localFilePath, const wchar_t *verb)
	{
		wtw::CFile opFile;

		if (!opFile.open(localFilePath, 0, 1, 1, 1, NULL, 1))
			return E_FAIL;

		HRESULT ret = downloadFile(fullUri, &opFile, 0, verb);
		opFile.close();
		return ret;
	};

	HRESULT CInternetHttp::downloadFile(const wchar_t *fullUri, wtw::CBuffer & buffer, const wchar_t *verb)
	{
		buffer.disposeBuffers();
		return downloadFile(fullUri, &buffer, 1, verb);
	};

	HRESULT CInternetHttp::makeHttpRequest(
		const wchar_t * verb, const wchar_t * url, wtw::CFile & openedFile,
		const wchar_t * referer, DWORD flags, 
		const wchar_t * getHeaderName, wchar_t * pHeaderBuffer, DWORD iHeaderBuffer, 
		int * pResponseCode)
	{
		HRESULT ret;

		if (FAILED(ret = prepareHttpRequest(verb, url, referer, flags, getHeaderName, pHeaderBuffer, iHeaderBuffer, pResponseCode)))
			return ret;

		curl_easy_setopt(m_hNet, CURLOPT_WRITEDATA, &openedFile);
		curl_easy_setopt(m_hNet, CURLOPT_WRITEFUNCTION, &_writeDataToFile);

		if (SUCCEEDED(ret = ctoh(curl_easy_perform(m_hNet))))
		{
			ret = finishHttpRequest(flags, pResponseCode, getHeaderName, pHeaderBuffer, iHeaderBuffer);
		}

		return ret;
	};

	HRESULT CInternetHttp::makeHttpRequest(
		const wchar_t * verb, const wchar_t * url, wtw::CBuffer & buffer,
		const wchar_t * referer, DWORD flags, 
		const wchar_t * getHeaderName, wchar_t * pHeaderBuffer, DWORD iHeaderBuffer, 
		int * pResponseCode)
	{
		HRESULT ret;

		if (FAILED(ret = prepareHttpRequest(verb, url, referer, flags, getHeaderName, pHeaderBuffer, iHeaderBuffer, pResponseCode)))
			return ret;

		curl_easy_setopt(m_hNet, CURLOPT_WRITEDATA, &buffer);
		curl_easy_setopt(m_hNet, CURLOPT_WRITEFUNCTION, &_writeDataToBuffer);

		if (SUCCEEDED(ret = ctoh(curl_easy_perform(m_hNet))))
		{
			ret = finishHttpRequest(flags, pResponseCode, getHeaderName, pHeaderBuffer, iHeaderBuffer);
		}
		return ret;
	};

	HRESULT CInternetHttp::finishHttpRequest(DWORD flags, 
		int * pResponseCode, const wchar_t * getHeaderName, wchar_t * pHeaderBuffer, DWORD iHeaderBuffer)
	{
		HRESULT ret = S_OK;
		if (pResponseCode)
		{
			curl_easy_getinfo(m_hNet, CURLINFO_RESPONSE_CODE, pResponseCode);
		}

		if (getHeaderName && pHeaderBuffer && iHeaderBuffer)
		{
			for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
			{
				if (_wcsicmp(it->name.c_str(), getHeaderName) == 0)
				{
					wcsncpy(pHeaderBuffer, it->value.c_str(), iHeaderBuffer);
				}
			}
		}
		return ret;
	}

	HRESULT CInternetHttp::prepareHttpRequest(
		const wchar_t * verb, const wchar_t * url, 
		const wchar_t * referer, DWORD flags, 
		const wchar_t * getHeaderName, wchar_t * pHeaderBuffer, DWORD iHeaderBuffer, 
		int * pResponseCode)
	{
		char *tmp;
		curl_slist *sl = NULL;

		m_headers.clear();

		tmp = wtw::CConv::wtou(url);
		curl_easy_setopt(m_hNet, CURLOPT_URL, tmp);
		wtw::CConv::release(tmp);

		if (NULL == m_postDataPtr && verb != NULL && 0 == wcscmp(L"POST", verb))
		{
			return E_NOT_VALID_STATE;
		}

		curl_easy_setopt(m_hNet, CURLOPT_MAXREDIRS, 10L);
		curl_easy_setopt(m_hNet, CURLOPT_FOLLOWLOCATION, (flags & NET_FLAG_NO_AUTO_REDIRECT) ? 0L : 1L);
		//curl_easy_setopt(m_hNet, CURLOPT_SSL_VERIFYPEER, 0L);
		//curl_easy_setopt(pNet, CURLOPT_SSL_VERIFYHOST, 0L);

		curl_easy_setopt(m_hNet, CURLOPT_POSTFIELDS, m_postDataPtr);
		curl_easy_setopt(m_hNet, CURLOPT_POSTFIELDSIZE, m_postDataLen);

		if (verb == NULL)
		{
			curl_easy_setopt(m_hNet, CURLOPT_HTTPGET, 1L);
		}
		else
		{
			if (0 == wcscmp(verb, L"GET"))
			{
				curl_easy_setopt(m_hNet, CURLOPT_HTTPGET, 1L);
			}
			else if (0 == wcscmp(verb, L"POST"))
			{
				curl_easy_setopt(m_hNet, CURLOPT_HTTPPOST, 1L);
			}
			else if (0 == wcscmp(verb, L"PUT"))
			{
				curl_easy_setopt(m_hNet, CURLOPT_PUT, 1L);
			}
			else if (0 == wcscmp(verb, L"HEAD"))
			{
				curl_easy_setopt(m_hNet, CURLOPT_NOBODY, 1L);
			}
			else
			{
				char *m = wtw::CConv::wtou(verb);
				curl_easy_setopt(m_hNet, CURLOPT_CUSTOMREQUEST, m);
				wtw::CConv::release(m);
			}
		}

		if (m_cookies.size())
		{
			wtw::CString cookie;
			cookie += L"Cookie: ";

			int loop = 0;
			for (std::deque<httpPairW>::iterator it = m_cookies.begin(); it != m_cookies.end(); ++it)
			{
				loop++;
				//wchar_t b[2048] = {0};
				//wnsprintf(b, 2048, L"%s=%s", it->name.c_str(), it->value.c_str());

				cookie += it->name;
				cookie += L"=";
				cookie += it->value;

				if (loop != m_cookies.size())
				{
					cookie += L"; ";
				}
			}

			tmp = wtw::CConv::wtou(cookie.c_str());
			sl = curl_slist_append(sl, tmp);
			wtw::CConv::release(tmp);
		}

		if (m_rqHeaders.size())
		{
			wtw::CString tmp;

			for (std::deque<httpPairW>::iterator it = m_rqHeaders.begin(); it != m_rqHeaders.end(); ++it)
			{
				tmp = it->name;
				tmp += L": ";
				tmp += it->value;

				char *u = wtw::CConv::wtou(tmp.c_str());
				sl = curl_slist_append(sl, u);
				wtw::CConv::release(u);
			}
		}

		if (referer)
		{
			wtw::CString ref;
			ref += L"Referer: ";
			ref += referer;

			tmp = wtw::CConv::wtou(ref.c_str());
			sl = curl_slist_append(sl, tmp);
			wtw::CConv::release(tmp);
		}

		if (m_postDataLen && m_postDataPtr)
		{
			tmp = wtw::CConv::wtou(L"Content-Type: application/x-www-form-urlencoded\r\n");
			sl = curl_slist_append(sl, tmp);
			wtw::CConv::release(tmp);
		}

		curl_easy_setopt(m_hNet, CURLOPT_HTTPHEADER, sl);

		setPostDataPtr(NULL, 0);

		curl_easy_setopt(m_hNet, CURLOPT_HEADERDATA, this);
		curl_easy_setopt(m_hNet, CURLOPT_HEADERFUNCTION, &_writeHeader);

		/*
		if (getHeaderName)
		{
			ULONG i = 0;
			wsprintf (pHeaderBuffer, L"%s", getHeaderName);
			ULONG l = iHeaderBuffer;
			if (!HttpQueryInfoW(hRequest, HTTP_QUERY_CUSTOM, pHeaderBuffer, &l, &i))
			{
				pHeaderBuffer[0] = 0;
			}
			else
			{
				iHeaderBuffer = l;
			}
		}
		*/

		return S_OK;
	}

	size_t CInternetHttp::_writeDataToFile( char *b, size_t s, size_t nmemb, void *userdata)
	{
		wtw::CFile *ptr = (wtw::CFile*)userdata;
		size_t r = s * nmemb;
		DWORD p;

		ptr->write(b, r, p);
		return r;
	}

	size_t CInternetHttp::_writeDataToBuffer( char *b, size_t s, size_t nmemb, void *userdata)
	{
		wtw::CBuffer *ptr = (wtw::CBuffer*)userdata;
		size_t r = s * nmemb;
		ptr->push((BYTE*)b, r);
		return r;
	}

	HRESULT CInternetHttp::getResponseHeader(const wchar_t * name, wtw::CString & value)
	{
		for (std::deque<httpPairW>::iterator it = m_headers.begin();
			it != m_headers.end(); ++it)
		{
			if (0 == wcsicmp(it->name, name))
			{
				value = it->value;
				return S_OK;
			}
		}
		return E_NOT_SET;
	}

	HRESULT CInternetHttp::addRequestHeader(const wchar_t * name, const wchar_t * value)
	{
		httpPairW p { name, value };
		m_rqHeaders.push_back(p);
		return m_rqHeaders.size();
	}

	HRESULT CInternetHttp::clearRequestHeaders()
	{
		m_rqHeaders.clear();
		return S_OK;
	}

	size_t CInternetHttp::_writeHeader( char *b, size_t s, size_t nmemb, void *userdata)
	{
		CInternetHttp *ptr = (CInternetHttp*)userdata;
		size_t r = s * nmemb;

		wchar_t * tmp = wtw::CConv::utow(b, r);
		wchar_t *v = wcschr(tmp, L':');
		if (v)
		{
			*v = 0;
			*v++;
			v = const_cast<wchar_t*>(wtw::trim(v));

			if (_wcsnicmp(tmp, L"Set-Cookie", wcslen(L"Set-Cookie")) == 0)
			{
				ptr->cookieAddOrEdit(tmp, v);
			}

			httpPairW h;

			h.name = tmp;
			h.value = v;

			ptr->m_headers.push_back(h);
		}

		wtw::CConv::release(tmp);
		return r;
	}

};