/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/Guid.h>
#include "rpcdce.h"

//#pragma warning (disable: 4244) // possible loss of data
#pragma warning (disable: 4267) // possible loss of data
#pragma comment (lib, "Rpcrt4.lib")

namespace wtw
{
	CGuid::CGuid()
	{
	}

	void* CGuid::_gen()
	{
		UUID* pUUID = new UUID;

		if (pUUID != NULL)
		{
			HRESULT hr;
			hr = UuidCreate(pUUID);
			if (hr == RPC_S_OK)
			{
				return pUUID;
			}
			delete pUUID;
		}
		return NULL;
	}

	HRESULT CGuid::release(char * regGuid)
	{
		delete [] regGuid;
		return S_OK;
	}

	HRESULT CGuid::release(wchar_t * regGuid)
	{
		delete [] regGuid;
		return S_OK;
	}

	HRESULT CGuid::genGuid(wchar_t ** regGuid)
	{
		*regGuid = NULL;
		unsigned short *sTemp;
		UUID* pUUID = reinterpret_cast<UUID*>(_gen());

		if (pUUID != NULL)
		{
			HRESULT hr = UuidToStringW(pUUID, &sTemp);
			if (hr == RPC_S_OK)
			{
				*regGuid = new wchar_t[64]();
				wcscpy(*regGuid, (wchar_t*)sTemp);
				RpcStringFreeW(&sTemp);
			}
			delete pUUID;
			return hr;
		}
		return E_FAIL;
	}

	HRESULT CGuid::genGuid(char ** regGuid)
	{
		*regGuid = NULL;
		unsigned char *sTemp;
		UUID* pUUID = reinterpret_cast<UUID*>(_gen());

		if (pUUID != NULL)
		{
			HRESULT hr = UuidToStringA(pUUID, &sTemp);
			if (hr == RPC_S_OK)
			{
				*regGuid = new char[64]();
				strcpy(*regGuid, (char*)sTemp);
				RpcStringFreeA(&sTemp);
			}
			delete pUUID;
			return hr;
		}
		return E_FAIL;
	}
}
