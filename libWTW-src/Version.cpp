#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Globals.h>
#include <cpp/Version.h>

OSVERSIONINFOEX wtw::SystemVersion::m_osve;

bool wtw::SystemVersion::isEight()
{
	return (m_osve.dwMajorVersion == 6 && m_osve.dwMinorVersion == 2) ? true : false;
}

bool wtw::SystemVersion::isEightPlus()
{
	return (m_osve.dwMajorVersion == 6 && m_osve.dwMinorVersion >= 2) ? true : (m_osve.dwMajorVersion > 6) ? true : false;
}

bool wtw::SystemVersion::isSeven()
{
	return (m_osve.dwMajorVersion == 6 && m_osve.dwMinorVersion == 1) ? true : false;
}

bool wtw::SystemVersion::isSevenPlus()
{
	return (m_osve.dwMajorVersion == 6 && m_osve.dwMinorVersion >= 1) ? true : (m_osve.dwMajorVersion > 6) ? true : false;
}

bool wtw::SystemVersion::isVista()
{
	return (m_osve.dwMajorVersion == 6 && m_osve.dwMinorVersion == 0) ? true : false;
}

bool wtw::SystemVersion::isVistaPlus()
{
	return (m_osve.dwMajorVersion >= 6) ? true : false;
}

bool wtw::SystemVersion::isXP()
{
	return (m_osve.dwMajorVersion == 5 && m_osve.dwMinorVersion == 1) ? true : false;
}

bool wtw::SystemVersion::isXPPlus()
{
	return (m_osve.dwMajorVersion == 5 && m_osve.dwMinorVersion >= 1) ? true : (m_osve.dwMajorVersion > 5) ? true : false;
}

DWORD wtw::SystemVersion::getMajor()
{
	return m_osve.dwMajorVersion;
}

DWORD wtw::SystemVersion::getMinor()
{
	return m_osve.dwMinorVersion;
}

DWORD wtw::SystemVersion::getRevision()
{
	return m_osve.dwBuildNumber;
}

DWORD wtw::SystemVersion::getServicePackLevel()
{
	return m_osve.wServicePackMajor;
}

bool wtw::SystemVersion::isHomeEdition()
{
	return (m_osve.wSuiteMask & VER_SUITE_PERSONAL) ? true : false;
}

bool wtw::SystemVersion::isServer()
{
	return (m_osve.wProductType != VER_NT_WORKSTATION) ? true : false;
}

bool wtw::SystemVersion::isDomainController()
{
	return (m_osve.wProductType == VER_NT_DOMAIN_CONTROLLER) ? true : false;
}

bool wtw::SystemVersion::isWorkstation()
{
	return (m_osve.wProductType == VER_NT_WORKSTATION) ? true : false;
}