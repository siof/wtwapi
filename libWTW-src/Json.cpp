/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Json.h>
#include <cpp/Globals.h>
#include <cpp/Conv.h>

#include "libJSON/jansson.h" 

#pragma warning (disable: 4244) // possible loss of data

wtw::CJsonIter::CJsonIter(void *iter, wtw::CJson *pObj)
{
	m_iter = iter;
	m_pObj = pObj;
}

wtw::CJsonIter::~CJsonIter()
{
}

void wtw::CJsonIter::release()
{
	delete this;
}

bool wtw::CJsonIter::next()
{
	m_iter = json_object_iter_next((json_t*)m_pObj, m_iter);
	return m_iter ? true : false;
}

const char *wtw::CJsonIter::key()
{
	return json_object_iter_key(m_iter);
}

wtw::CJson * wtw::CJsonIter::value()
{
	return (wtw::CJson*)json_object_iter_value(m_iter);
}

wtw::CJson::CJson()
{
}

wtw::CJson::~CJson()
{
}

wtw::CJsonIter *wtw::CJson::getIter()
{
	return new wtw::CJsonIter(json_object_iter((json_t*)this), this);
}

bool wtw::CJson::isArray()
{
	return json_is_array((json_t*)this);
}

bool wtw::CJson::isObject()
{
	return json_is_object((json_t*)this);
}

bool wtw::CJson::isString()
{
	return json_is_string((json_t*)this);
}

bool wtw::CJson::isTrue()
{
	return json_is_true((json_t*)this);
}

bool wtw::CJson::isFalse()
{
	return json_is_false((json_t*)this);
}

bool wtw::CJson::isInteger()
{
	return json_is_integer((json_t*)this);
}

bool wtw::CJson::isBoolean()
{
	return json_is_boolean((json_t*)this);
}

bool wtw::CJson::isNumber()
{
	return json_is_number((json_t*)this);
}

bool wtw::CJson::isReal()
{
	return json_is_real((json_t*)this);
}

wtw::CJson * wtw::CJson::load(const char* input)
{
	json_error_t e;
	return (CJson*)json_loads(input, 0, &e);
}

wtw::CJson * wtw::CJson::load(const wchar_t* input)
{
	json_error_t e;

	char *inputu = wtw::CConv::wtou(input);
	json_t* r = json_loads(inputu, 0, &e);
	wtw::CConv::release(inputu);

	return (CJson*)r;
}

void wtw::CJson::decref(wtw::CJson * o)
{
	json_decref(o);
}

wtw::CJson * wtw::CJson::getAt(size_t index)
{
	if (json_is_array(this))
		return (CJson*)json_array_get(this, index);
	else
		return NULL;
}

wtw::CJson * wtw::CJson::operator [] (size_t index)
{
	return getAt(index);
}

wtw::CJson * wtw::CJson::operator [] (const char *str)
{
	return find(str);
}

const char* wtw::CJson::c_str()
{
	return json_string_value(this);
}

__int64 wtw::CJson::c_int()
{
	return json_integer_value(this);
}

double wtw::CJson::c_real()
{
	return json_real_value(this);
}

bool wtw::CJson::c_bool()
{
	return json_is_true(this);
}

size_t wtw::CJson::size()
{
	if (json_is_array(this))
		return json_array_size(this);
	else if (json_is_object(this))
		return json_object_size(this);
	else
		return 0;
}

wtw::CJson * wtw::CJson::find(const char *name)
{
	return (CJson*)json_object_get(this, name);
}