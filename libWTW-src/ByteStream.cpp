/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/ByteStream.h>
#include <cpp/Conv.h>

namespace wtw
{

	CByteStream::CByteStream(void)
	{
		m_position = 0;
	}

	CByteStream::CByteStream(void *b, size_t len)
	{
		CBuffer::replaceBuffer((BYTE*)b, len);
		m_position = 0;
	}

	CByteStream::~CByteStream(void)
	{
	}

	WTW_PTR CByteStream::eof()
	{
		if (m_position >= getLength())
		{
			return S_OK;
		}
		else
		{
			return S_FALSE;
		}
	}

	void CByteStream::rewind()
	{
		m_position = 0;
	}

	WTW_PTR CByteStream::seek(int offset)
	{
		int n = m_position + offset;
		
		if (n > getLength() || n < 0)
		{
			return E_FAIL;
		}

		m_position = n;
		return S_OK;
	}

	WTW_PTR CByteStream::getInt(int & i)
	{
		if (m_position + sizeof(i) <= getLength())
		{
			memcpy(&i, getBuffer() + m_position, sizeof(i));
			m_position += sizeof(i);
			return S_OK;
		}
		return E_FAIL;
	}

	WTW_PTR CByteStream::getDword(DWORD & i)
	{
		if (m_position + sizeof(i) <= getLength())
		{
			memcpy(&i, getBuffer() + m_position, sizeof(i));
			m_position += sizeof(i);
			return S_OK;
		}
		return E_FAIL;
	}

	WTW_PTR CByteStream::getLong(__int64 & i)
	{
		if (m_position + sizeof(i) <= getLength())
		{
			memcpy(&i, getBuffer() + m_position, sizeof(i));
			m_position += sizeof(i);
			return S_OK;
		}
		return E_FAIL;
	}

	WTW_PTR CByteStream::getByte(BYTE & i)
	{
		if (m_position + sizeof(i) <= getLength())
		{
			memcpy(&i, getBuffer() + m_position, sizeof(i));
			m_position += sizeof(i);
			return S_OK;
		}
		return E_FAIL;
	}

	WTW_PTR CByteStream::getVarStringUtf8(wtw::CString & str, int lenLen)
	{
		if (m_position + lenLen > getLength())
			return E_FAIL;

		int len = 0;
		memcpy(&len, getBuffer() + m_position, lenLen);
		m_position += lenLen;

		if (m_position + len > getLength())
			return E_FAIL;

		wchar_t *w = wtw::CConv::utow((char*)getBuffer() + m_position, len);
		str.append(w);
		wtw::CConv::release(w);
		m_position += len;
		return S_OK;
	}

	void CByteStream::putInt(const int val)
	{
		if (m_position == getLength())
		{
			push((BYTE*)&val, sizeof(val));
			m_position += sizeof(val);
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putByte(const char val)
	{
		if (m_position == getLength())
		{
			push((BYTE*)&val, sizeof(val));
			m_position += sizeof(val);
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putData(const void *data, int len)
	{
		if (m_position == getLength())
		{
			push((BYTE*)data, len);
			m_position += len;
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putLong(const __int64 val)
	{
		if (m_position == getLength())
		{
			push((BYTE*)&val, sizeof(val));
			m_position += sizeof(val);
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putString(const char* val)
	{
		size_t l = strlen(val);

		if (m_position == getLength())
		{
			push((BYTE*)val, l);
			m_position += l;
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putString(const wchar_t* val)
	{
		size_t l = wcslen(val) * sizeof(wchar_t);

		if (m_position == getLength())
		{
			push((BYTE*)val, l);
			m_position += l;
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putStringUtf8(const wchar_t* val)
	{
		char *u8 = CConv::wtou(val);
		putString(u8);
		CConv::release(u8);
	}

	void CByteStream::putVarString(const wchar_t* val, int lenLen)
	{
		lenLen = min(lenLen, sizeof(lenLen));
		size_t l = wcslen(val);

		if (m_position == getLength())
		{
			push((BYTE*)&l, lenLen);
			push((BYTE*)val, l * sizeof(wchar_t));
			m_position += (l * sizeof(wchar_t)) + lenLen;
		}
		else
		{
			// case not yet supported ;P
		}
	}

	void CByteStream::putVarStringUtf8(const wchar_t* val, int lenLen)
	{
		char *u8 = CConv::wtom(val);
		putVarString(u8, lenLen);
		CConv::release(u8);
	}

	void CByteStream::putVarString(const char* val, int lenLen)
	{
		lenLen = min(lenLen, sizeof(lenLen));
		size_t l = strlen(val);

		if (m_position == getLength())
		{
			push((BYTE*)&l, lenLen);
			push((BYTE*)val, l);
			m_position += l + lenLen;
		}
		else
		{
			// case not yet supported ;P
		}
	}
};