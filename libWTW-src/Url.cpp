#include "stdafx.h"
#include <cpp/Url.h>
#include <cpp/Buffer.h>
#include <math.h>

#pragma warning(disable: 4244)	// possible loss of data
#pragma warning(disable: 4267)  // possible loss of data

wtw::CUrl::CUrl()
{
}

wtw::CUrl::~CUrl()
{
}

void inline _pushb(wtw::CBuffer *pBuffer, const char *c, int l, int bWide)
{
	static int null = 0;
	for (int i = 0; i < l; ++i)
	{
		pBuffer->push((BYTE*)&(c[i]), 1);	
		if (bWide)
		{
			pBuffer->push((BYTE*)&null, 1);
		}
	}
}

int twoHex2Int(int c1, int c2) 
{
    int hi = c1, lo = c2;
    
    if (L'0' <= hi && hi <= L'9')
		hi -= L'0';
    else if (L'a' <= hi && hi <= L'f') 
		hi -= (L'a' - 10);
    else if (L'A' <= hi && hi <= L'F') 
		hi -= (L'A' - 10);
    
    if (L'0' <= lo && lo <= L'9')      
		lo -= '0';
    else if (L'a' <= lo && lo <= L'f') 
		lo -= (L'a' - 10);
    else if (L'A' <= lo && lo <= L'F') 
		lo -= (L'A' - 10);

    return lo + 16 * hi;
}

static HRESULT _encode(const char *src, char **dst, int bWide)
{
	wtw::CBuffer destString;
    int len;
	
	if (bWide)
		len = wcslen((const wchar_t*)src) * sizeof(wchar_t);
	else
		len = strlen(src);

	if (!src || !dst)
		return E_INVALIDARG;

	*dst = NULL;

	if (!len)
		return E_INVALIDARG;

    for(int i = 0; i < len; i++)
    {
        wchar_t c = src[i];
        if ((c >= 48 && c<= 57) || 
			(c >= 65 && c<= 90) || 
			(c >= 97 && c<= 122) ||
			 c == '.' || c == '~' || 
			 c == '_' || c == '-') 
		{
            _pushb(&destString, (const char*)&c, 1, bWide);
        } 
        else if(c == ' ') 
		{
			_pushb(&destString, "+", 1, bWide);
        } 
        else 
		{
			char temp[8] = {0};
            sprintf(temp, "%%%02X", (unsigned int)(c & 0xFF));
            _pushb(&destString, temp, strlen(temp), bWide);
        }

		if (bWide)
		{
			i++;
		}
    }
	*dst = (char*)destString.detach(len, NULL);
	return S_OK;
}

static HRESULT _decode(const char *src, char **dst, int bWide)
{
	wtw::CBuffer destString;
	int shift = 1, len;
	if (bWide)
	{
		len = wcslen((const wchar_t*)src) * sizeof(wchar_t);
		shift = 2;
	}
	else
		len = strlen(src);

	if (!src || !dst)
		return E_INVALIDARG;

	*dst = NULL;

	if (!len)
		return E_INVALIDARG;

	for(int i = 0; i < len; i += shift)
    {
        if(src[i] == '%') 
		{
            i += shift;
            if (isxdigit(src[i]) && isxdigit(src[i + shift])) 
			{
                const char t = (const char) twoHex2Int(src[i], src[i + shift]);
                _pushb(&destString, &t, 1, bWide); 
				i += shift;
            }
        }
        else if(src[i] == L'+') 
		{
			_pushb(&destString, " ", 1, bWide);
        }
        else 
		{
			_pushb(&destString, (const char*)&(src[i]), 1, bWide);
        }
    }
	*dst = (char*)destString.detach(len, NULL);
    return S_OK;
}


HRESULT wtw::CUrl::encode(const char *src, char **dst)
{
	return _encode(src, dst, 0);
}

HRESULT wtw::CUrl::encode(const wchar_t *src, wchar_t **dst)
{
	return _encode((const char*)src, (char**)dst, 1);
}


HRESULT wtw::CUrl::decode(const char *src, char **dst)
{
	return _decode(src, dst, 0);
}

HRESULT wtw::CUrl::decode(const wchar_t *src, wchar_t **dst)
{
	return _decode((const char*)src, (char**)dst, 1);
}

void wtw::CUrl::release(const char* r)
{
	delete [] r;
}

void wtw::CUrl::release(const wchar_t* r)
{
	delete [] r;
}

HRESULT wtw::CUrl::getHostFromUrl(const wchar_t *uri, wtw::CString &host)
{
	const wchar_t * h = wcsstr(uri, L"://");

	if (NULL == h)
		return E_INVALIDARG;

	h += 3;

	const wchar_t * p = wcsstr(h, L"/");

	if (NULL == p)
	{
		host = h;
		return S_OK;
	}

	host.clear();
	host.append((wchar_t*)h, (p - h));
	return S_OK;
}
