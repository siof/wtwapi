/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/ContactDraw.h>

namespace wtw {

	CContactDraw::CContactDraw(wtwContactListItemEvent *pF)
	{
		m_pEv = pF;
	}

	void CContactDraw::drawBackground()
	{
		wtwContactListStdDraw dr;
		initStruct(dr);

		dr.pEvent		= m_pEv;
		dr.whatToDraw	= CTL_STD_ELEMENT_BACKGROUND;

		(m_pEv->fnDrawStandardElement)(
			(WTW_PARAM)&dr, 
			(WTW_PARAM)NULL, 
			m_pEv->cbDataDrawStandardElement);
	};

	void CContactDraw::drawText(
		const wchar_t * text, int x, int y, int cx, int cy, int bMultiline, int bMain)
	{
		wtwContactListStdElementText txt;
		initStruct(txt);
		wtwContactListStdDraw dr;
		initStruct(dr);

		dr.pEvent		= m_pEv;

		txt.text = text;

		txt.x	= x;
		txt.y	= y;
		txt.cx	= cx;
		txt.cy	= cy;

		if (bMultiline)
			txt.flags = CSE_TEXT_FLAG_MULTILINE;

		if (bMain)
			dr.whatToDraw = CTL_STD_ELEMENT_MAIN_TEXT;
		else
			dr.whatToDraw = CTL_STD_ELEMENT_DESC_TEXT;

		(m_pEv->fnDrawStandardElement)(
			(WTW_PARAM)&dr, 
			(WTW_PARAM)&txt, 
			m_pEv->cbDataDrawStandardElement);
	};

	void CContactDraw::calcTextRect(
		const wchar_t * text, int & x, int & y, int & cx, int & cy, int bMultiline, int bMain)
	{
		wtwContactListStdElementText txt;
		initStruct(txt);
		wtwContactListStdDraw dr;
		initStruct(dr);

		dr.pEvent		= m_pEv;

		txt.text = text;

		txt.x	= x;
		txt.y	= y;
		txt.cx	= cx;
		txt.cy	= cy;

		if (bMultiline)
			txt.flags = CSE_TEXT_FLAG_MULTILINE;

		txt.flags |= CSE_TEXT_FLAG_CALC_ONLY;

		if (bMain)
			dr.whatToDraw = CTL_STD_ELEMENT_MAIN_TEXT;
		else
			dr.whatToDraw = CTL_STD_ELEMENT_DESC_TEXT;

		(m_pEv->fnDrawStandardElement)(
			(WTW_PARAM)&dr, 
			(WTW_PARAM)&txt, 
			m_pEv->cbDataDrawStandardElement);

		x	= txt.x;
		y	= txt.y;
		cx	= txt.cx;
		cy	= txt.cy;
	};
}