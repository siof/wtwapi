/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <sqlite3.h>
#include <cpp/core.h>
#include <cpp/database.h>

#pragma warning (disable: 4267)
#pragma warning (disable: 4244)

namespace wtw
{
	namespace db
	{
		CConnection::CConnection() : m_db(NULL), m_bInTransaction(0)
		{
		}
		
		CConnection::CConnection(const wchar_t * dbPath) : m_db(NULL)
		{
			open(dbPath);
		}

		CConnection::~CConnection()
		{
			if (m_db)
				close();
		}

		bool	CConnection::isOpened()
		{
			return m_db ? true : false;
		}

		HRESULT	CConnection::releaseMemory()
		{
			if (!m_db)
				return E_NOT_VALID_STATE;

			return (SQLITE_OK == sqlite3_db_release_memory(m_db)) ? S_OK : E_FAIL;
		}

		HRESULT CConnection::transactionBegin()
		{
			if (NULL == m_db)
				return E_UNEXPECTED;

			m_bInTransaction ++;

			if (1 == m_bInTransaction)
			{
				HRESULT r; 
				if (SUCCEEDED(r = executeNoRead(L"BEGIN TRANSACTION;")))
				{
					m_bInTransaction = 1;
				}
				return r;
			}
			else
				return E_NOT_VALID_STATE;
		}

		HRESULT CConnection::transactionCommit()
		{
			if (NULL == m_db)
				return E_UNEXPECTED;

			m_bInTransaction--;

			if (m_bInTransaction < 0)
				m_bInTransaction = 0;

			if (0 == m_bInTransaction)
			{
				HRESULT r; 
				if (FAILED(r = executeNoRead(L"COMMIT TRANSACTION;")))
				{
					if (sqlite3_get_autocommit(m_db) == 0)
					{
						executeNoRead(L"ROLLBACK TRANSACTION;");
					}
				}
				return r;
			}
			else
				return E_NOT_VALID_STATE;
		}

		HRESULT CConnection::transactionRollback()
		{
			if (NULL == m_db)
				return E_UNEXPECTED;

			m_bInTransaction --;

			if (m_bInTransaction < 0)
				m_bInTransaction = 0;

			if (0 == m_bInTransaction)
			{
				HRESULT r; 
				if (SUCCEEDED(r = executeNoRead(L"ROLLBACK TRANSACTION;")))
				{
					m_bInTransaction = 0;
				}
				return r;
			}
			else
				return E_NOT_VALID_STATE;
		}

		HRESULT CConnection::inTrasaction()
		{
			if (NULL == m_db)
				return E_UNEXPECTED;

			if (m_bInTransaction)
				return S_OK;
			else
				return S_FALSE;
		}

		HRESULT CConnection::columnExists(const wchar_t *table, const wchar_t *column)
		{
			wchar_t sql[255];
			swprintf(sql, 255, L"PRAGMA table_info(%s);", table);

			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			while (r.read() == S_OK)
			{
				const wchar_t *cn;
				if (r.getResult(L"name", &cn) == S_OK && 0 == _wcsicmp(column, cn))
				{
					return S_OK;
				}
			}
			return S_FALSE;
		}

		HRESULT CConnection::executeNoRead(const wchar_t*sql)
		{
			return CCommand(*this, sql).executeNoRead();
		}

		HRESULT CConnection::executeAndRead(const wchar_t *sql, int & ret)
		{
			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return S_FALSE;
		}

		HRESULT CConnection::executeAndRead(const wchar_t *sql, long long & ret)
		{
			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return S_FALSE;
		}

		HRESULT CConnection::executeAndRead(const wchar_t *sql, double & ret)
		{
			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return S_FALSE;
		}

		HRESULT CConnection::executeAndRead(const wchar_t *sql, wtw::CString & ret)	
		{
			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			if (r.read() == S_OK)
				return r.getResult(0, ret, L"");
			else
				return S_FALSE;
		}

		HRESULT CConnection::executeAndRead(const wchar_t *sql, const wchar_t **ret)
		{
			CCommand cmd(*this, sql);
			CReader r = cmd.executeReader();

			if (r.read() == S_OK)
				return r.getResult(0, ret, NULL);
			else
				return S_FALSE;
		}
		
		HRESULT CConnection::open(const wchar_t *dbPath)
		{
			if(sqlite3_open16(dbPath, &m_db) != SQLITE_OK)
				return E_FAIL;
			else
			{
				m_bInTransaction = 0;
				return S_OK;
			}
		}

		HRESULT CConnection::close()
		{
			if (m_db)
			{
				if(sqlite3_close(m_db) != SQLITE_OK)
					return E_FAIL;
				else
				{
					m_bInTransaction = 0;
					m_db = NULL;
					return S_OK;
				}
			}
			else
				return S_FALSE;
		}

		HRESULT CConnection::getLastError(DWORD & code)
		{
			code = sqlite3_errcode(m_db);
			return S_OK;
		}

		HRESULT CConnection::getLastError(const wchar_t **ret)
		{
			*ret = (const wchar_t*)sqlite3_errmsg16(m_db);
			return S_OK;
		}

		HRESULT CConnection::getLastInsertId(unsigned long long &id)
		{
			if (!m_db)
				return E_NOT_VALID_STATE;

			id = sqlite3_last_insert_rowid(m_db);;
			return S_OK;
		}

		HRESULT CConnection::getLastInsertId(long long &id)
		{
			if (!m_db)
				return E_NOT_VALID_STATE;

			id = sqlite3_last_insert_rowid(m_db);;
			return S_OK;
		}
	
		HRESULT CConnection::getLastInsertId(unsigned int &id)
		{
			if (!m_db)
				return E_NOT_VALID_STATE;

			id = sqlite3_last_insert_rowid(m_db);;
			return S_OK;
		}

		HRESULT CConnection::getLastInsertId(int &id)
		{
			if (!m_db)
				return E_NOT_VALID_STATE;

			id = sqlite3_last_insert_rowid(m_db);;
			return S_OK;
		}

		CCommand::CCommand(CConnection & con, const wchar_t*sql) : m_con(con), m_statament(NULL), m_refs(0)
		{
			prepare(sql);
		}

		CCommand::CCommand(CConnection & con) : m_con(con), m_statament(NULL), m_refs(0)
		{
		}

		CCommand::~CCommand()
		{
			finalize();
		}

		HRESULT CCommand::prepare(const wchar_t *sql)
		{
			finalize();

			const void *tail = NULL;
			if(sqlite3_prepare16(m_con.m_db, sql, -1, &m_statament, &tail) != SQLITE_OK)
				return E_FAIL;

			m_argc = sqlite3_column_count(m_statament);
			return S_OK;
		}

		HRESULT CCommand::finalize()
		{
			if (m_statament)
			{
				sqlite3_finalize(m_statament);
				m_statament = NULL;
				return S_OK;
			}
			return S_FALSE;
		}

		HRESULT CCommand::executeNoRead()
		{
			return executeReader().read();
		}

		CReader CCommand::executeReader() 
		{
			return CReader(this);
		}

		HRESULT CCommand::bind(int index) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if(sqlite3_bind_null(m_statament, index) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const int & data) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if(sqlite3_bind_int(m_statament, index, data) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const long long & data) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if(sqlite3_bind_int64(m_statament, index, data) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const double & data) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if(sqlite3_bind_double(m_statament, index, data) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const wchar_t *data, int dataLen) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if (!data)
				return E_INVALIDARG;

			if(sqlite3_bind_text16(m_statament, index, data, (dataLen == -1) ? (wcslen(data) * sizeof(wchar_t)) : (dataLen * sizeof(wchar_t)), NULL) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const char *data, int dataLen)
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if (!data)
				return E_INVALIDARG;

			if(sqlite3_bind_text(m_statament, index, data, (dataLen == -1) ? strlen(data) : dataLen, NULL) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::bind(int index, const void *data, int dataLen) 
		{
			if (!m_statament)
				return E_NOT_VALID_STATE;

			if (!data)
				return E_INVALIDARG;

			if(sqlite3_bind_blob(m_statament, index, data, dataLen, NULL) != SQLITE_OK)
				return E_FAIL;
			else
				return S_OK;
		}

		HRESULT CCommand::executeAndRead(int & ret)
		{
			CReader r = executeReader();
			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return E_FAIL;
		}

		HRESULT CCommand::executeAndRead(long long & ret)
		{
			CReader r = executeReader();
			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return E_FAIL;
		}

		HRESULT CCommand::executeAndRead(double & ret)
		{
			CReader r = executeReader();
			if (r.read() == S_OK)
				return r.getResult(0, ret);
			else
				return E_FAIL;
		}

		HRESULT CCommand::executeAndRead(wtw::CString & ret)
		{
			CReader r = executeReader();
			if (r.read() == S_OK)
				return r.getResult(0, ret, L"");
			else
				return E_FAIL;
		}

		CReader::CReader(CCommand *cmd) : m_cmd(cmd) 
		{
			m_rowCount = 0;
			m_intptr = 0;
			++cmd->m_refs;
			count(m_rowCount);
		}

		CReader::CReader() : m_cmd(NULL) 
		{
			m_intptr = 0;
			m_rowCount = 0;
		}

		CReader::CReader(const CReader &copy) : m_cmd(copy.m_cmd) 
		{
			m_rowCount = 0;
			m_intptr = 0;
			if(m_cmd) 
				++m_cmd->m_refs;
			count(m_rowCount);
		}

		CReader::~CReader() 
		{
			close();
			releaseIntPointers(); // just to be sure
		}

		HRESULT CReader::getRowNum(int & i)
		{
			if (!m_cmd) 
				return E_NOT_VALID_STATE;

			if (!m_cmd->m_statament)
				return E_NOT_VALID_STATE;

			i = m_intptr;
			return S_OK;
		}

		HRESULT CReader::hasNext()
		{
			if (!m_cmd) 
				return E_NOT_VALID_STATE;

			if (!m_cmd->m_statament)
				return E_NOT_VALID_STATE;

			return (m_intptr < m_rowCount) ? S_OK : S_FALSE;
		}

		CReader& CReader::operator = (const CReader &copy) 
		{
			close();

			m_cmd = copy.m_cmd;
			if(m_cmd) 
				++m_cmd->m_refs;

			return *this;
		}

		HRESULT CReader::read() 
		{
			if (!m_cmd) 
				return E_NOT_VALID_STATE;

			if (!m_cmd->m_statament)
				return E_NOT_VALID_STATE;

			switch(sqlite3_step(m_cmd->m_statament)) 
			{
			case SQLITE_ROW:
				m_intptr++;
				return S_OK;
			case SQLITE_DONE:
				m_intptr++;
				return S_FALSE;
			default:
				return E_FAIL;
			}
		}

		HRESULT CReader::count(int & count)
		{
			if(!m_cmd) 
				return E_NOT_VALID_STATE;

			count = sqlite3_data_count(m_cmd->m_statament);
			return S_OK;
		}

		HRESULT CReader::reset() 
		{
			if(!m_cmd) 
				return E_NOT_VALID_STATE;

			if(sqlite3_reset(m_cmd->m_statament) != SQLITE_OK)
				return E_FAIL;

			m_intptr = 0;
			return S_OK;
		}

		HRESULT CReader::close() 
		{
			if(m_cmd) 
			{
				if(--m_cmd->m_refs == 0) 
				{
					sqlite3_reset(m_cmd->m_statament);
					releaseIntPointers();
				}

				m_cmd = NULL;
				return S_OK;
			}
			return E_FAIL;
		}

		HRESULT CReader::getResult(int index, int &result, int defaultVal) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				result = defaultVal;
				return E_NOT_SET;
			}

			result = sqlite3_column_int(m_cmd->m_statament, index);
			return S_OK;
		}

		HRESULT CReader::getResult(int index, long long &result, long long defaultVal) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				result = defaultVal;
				return E_NOT_SET;
			}

			result = sqlite3_column_int64(m_cmd->m_statament, index);
			return S_OK;
		}

		HRESULT CReader::getResult(int index, double &result, double defaultVal) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc) 
			{
				result = defaultVal;
				return E_NOT_SET;
			}

			result = sqlite3_column_double(m_cmd->m_statament, index);
			return S_OK;
		}
		
		HRESULT CReader::getResult(int index, const char** result, const char *defaultVal)
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				*result = defaultVal;
				return E_NOT_SET;
			}

			*result = (char*)sqlite3_column_text(m_cmd->m_statament, index);
			return S_OK;
		}

		HRESULT CReader::getResult(int index, const wchar_t** result, const wchar_t *defaultVal)
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				*result = defaultVal;
				return E_NOT_SET;
			}

			int rSize = sqlite3_column_bytes16(m_cmd->m_statament, index);
			const void *r = sqlite3_column_text16(m_cmd->m_statament, index);
			
			if (rSize > 0)
			{
				*result = new wchar_t[rSize + 1]();
				memcpy((void*)*result, r, rSize);
				m_toFree.push_back((void*)*result);
			}

			return S_OK;
		}

		void CReader::releaseIntPointers()
		{
			for(std::deque<void*>::iterator it = m_toFree.begin(); it != m_toFree.end(); ++it)
			{
				delete [] *it;
			}
			m_toFree.clear();
		}

		HRESULT CReader::getResult(int index, wtw::CString &result, const wtw::CString & defaultVal) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				result = defaultVal;
				return E_NOT_SET;
			}

			result.clear();
			result.append(
				(wchar_t*)sqlite3_column_text16(m_cmd->m_statament, index), 
				sqlite3_column_bytes16(m_cmd->m_statament, index) / sizeof(wchar_t));
			return S_OK;
		}

		HRESULT CReader::getResult(int index, void **data, int & dataLen) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
			{
				return E_NOT_SET;
			}

			void *rd = (void *)sqlite3_column_blob(m_cmd->m_statament, index);
			dataLen = sqlite3_column_bytes(m_cmd->m_statament, index);
			
			if (dataLen > 0)
			{
				*data = new char[dataLen + 1]();
				memcpy((void*)*data, rd, dataLen);
				m_toFree.push_back((void*)*data);
			}

			return S_OK;
		}

		HRESULT CReader::getColumnSize(int index, int & size) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index >= m_cmd->m_argc)
				return E_NOT_SET;

			size = sqlite3_column_bytes16(m_cmd->m_statament, index);
			return S_OK;
		}

		HRESULT CReader::getResult(const wchar_t *colName, void **data, int & dataLen) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i) 
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, data, dataLen);
					}
				}
			}
			return E_NOT_SET;
		}

		HRESULT CReader::getResult(const wchar_t *colName, int &result, int defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i)
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}
			result = defaultVal;
			return E_NOT_SET;
		}

		HRESULT CReader::getResult(const wchar_t *colName, long long &result, long long defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i)
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}
			result = defaultVal;
			return E_NOT_SET;
		}

		HRESULT CReader::getResult(const wchar_t *colName, double &result, double defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i)
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}
			result = defaultVal;
			return E_NOT_SET;
		}

		HRESULT CReader::getResult(const wchar_t *colName, const char **result, const char *defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i) 
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}

			*result = defaultVal;
			return E_NOT_SET;
		}

		HRESULT CReader::getResult(const wchar_t *colName, const wchar_t **result, const wchar_t *defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i) 
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}

			*result = defaultVal;
			return E_NOT_SET;
		}
		
		HRESULT CReader::getResult(const wchar_t *colName, wtw::CString &result, const wtw::CString & defaultVal) 
		{
			if (!colName)
				return E_INVALIDARG;

			int c;
			if (FAILED(getColumnCount(c)))
				return E_FAIL;

			for (int i = 0; i < c; ++i) 
			{
				const wchar_t *col = NULL;
				if (SUCCEEDED(getColumnName(i, &col)))
				{
					if (!wcscmp(colName, col))
					{
						return getResult(i, result, defaultVal);
					}
				}
			}
			result = defaultVal;
			return E_NOT_SET;
		}

		HRESULT CReader::getColumnCount(int &count) 
		{
			if(!m_cmd) 
			{
				count = 0;
				return E_FAIL;
			}

			count = sqlite3_column_count(m_cmd->m_statament);
			return S_OK;
		}

		HRESULT CReader::getColumnName(int index, wtw::CString & name) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index > (m_cmd->m_argc - 1)) 
				return E_NOT_SET;

			name = (const wchar_t*)sqlite3_column_name16(m_cmd->m_statament, index);
			return S_OK;
		}

		HRESULT CReader::getColumnName(int index, const wchar_t **name) 
		{
			if(!m_cmd) 
				return E_FAIL;

			if(index > (m_cmd->m_argc - 1)) 
				return E_NOT_SET;

			*name = (const wchar_t*)sqlite3_column_name16(m_cmd->m_statament, index);
			return S_OK;
		}

		CTransaction::CTransaction(CConnection &con, int bStart) : m_con(con), m_inTrans(0) 
		{
			if(bStart) 
				begin();
		}

		CTransaction::~CTransaction()
		{
			if (!this)
				return;

			if(m_inTrans) 
				rollback();
		}

		HRESULT CTransaction::begin()
		{
			if (m_inTrans)
				return S_FALSE;

			if (FAILED(m_con.executeNoRead(L"BEGIN;")))
				return E_FAIL;

			m_inTrans = 1;
			return S_OK;
		}
		
		HRESULT CTransaction::commit()
		{
			if (!m_inTrans)
				return S_FALSE;

			if (FAILED(m_con.executeNoRead(L"COMMIT;")))
				return E_FAIL;

			m_inTrans = 0;
			return S_OK;
		}

		HRESULT CTransaction::rollback()
		{
			if (!m_inTrans)
				return S_FALSE;

			if (FAILED(m_con.executeNoRead(L"ROLLBACK;")))
				return E_FAIL;

			m_inTrans = 0;
			return S_OK;
		}
	};
};