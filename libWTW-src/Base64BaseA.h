#pragma once

#include "Base64Common.h"

int base64_decode_blockA(const char* code_in, const int length_in, char* plaintext_out, base64_decodestate* state_in);
int base64_encode_blockA(const char* plaintext_in, int length_in, char* code_out, base64_encodestate* state_in);
int base64_encode_blockendA(char* code_out, base64_encodestate* state_in);
