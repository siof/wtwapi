#pragma once

#include "Base64Common.h"

int base64_decode_blockW(const wchar_t* code_in, const int length_in, char* plaintext_out, base64_decodestate* state_in);
int base64_encode_blockW(const char* plaintext_in, int length_in, wchar_t* code_out, base64_encodestate* state_in);
int base64_encode_blockendW(wchar_t* code_out, base64_encodestate* state_in);
