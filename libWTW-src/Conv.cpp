/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/Conv.h>

//#pragma warning (disable: 4244) // possible loss of data
#pragma warning (disable: 4267) // possible loss of data

namespace wtw
{
	int inline _alignto(int n, int a)
	{
		return n + (a - (n % a));
	}

	CConv::CConv()
	{
	}

	void CConv::release(char *p)
	{
		delete [] p;
	}

	void CConv::release(wchar_t *p)
	{
		delete [] p;
	}

	wchar_t * CConv::mtow(const char *in)
	{
		return _tow(in, CP_ACP);
	}

	char * CConv::wtom(const wchar_t *in)
	{
		return _tom(in, CP_ACP);
	}

	char * CConv::wtou(const wchar_t *in)
	{
		return _tom(in, CP_UTF8);
	}

	char * CConv::wtou(const wchar_t *in, int size)
	{
		return _tom(in, CP_UTF8, size);
	}

	wchar_t * CConv::utow(const char *in)
	{
		return _tow(in, CP_UTF8);
	}

	wchar_t * CConv::utow(const char *in, int size)
	{
		return _tow(in, CP_UTF8, size);
	}

	char * CConv::wtoi85592(const wchar_t *p)
	{
		return _tom(p, 28592);
	}

	wchar_t * CConv::i85592tow(const char *p)
	{
		return _tow(p, 28592);
	}

	wchar_t * CConv::_tow(const char *in, int cp, int size)
	{
		if (!in)
		{
			return new wchar_t[_alignto(1, 8)]();
		}

		int		l			= _alignto((size == -1 ? strlen(in) : size) + 1, 8);
		wchar_t *unicode	= new wchar_t [l]();

		::MultiByteToWideChar(
			cp, 0, 
			in, size, 
			unicode, l
			);

		return unicode;
	}

	char * CConv::_tom(const wchar_t *in, int cp, int size)
	{
		if (!in)
		{
			return new char [_alignto(1, 16)]();
		}

		int needLen = WideCharToMultiByte(
			cp, 0, 
			in, size, 
			0, 0, 
			NULL, NULL
			);

		int		l		= _alignto(needLen + 1, 16);
		char    *mb		= new char [l]();

		::WideCharToMultiByte(
			cp, 0, 
			in, size, 
			mb, l, 
			NULL, NULL
			);

		return mb;
	}

	char * CConv::wtow1252(const wchar_t *in)
	{
		return _tom(in, 1252);
	}

	wchar_t * CConv::w1252tow(const char *in)
	{
		return _tow(in, 1252);
	}
};