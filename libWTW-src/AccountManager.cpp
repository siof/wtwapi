/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/AccountManager.h>

namespace wtw
{
	CAccountManager::CAccountManager(const wchar_t * netClass, const wchar_t * netName)
	{
		wtwACMInterface ami;

		m_netClass = ami.netClass = netClass;
		ami.netName		= netName;

		ami.eventCallback	= &CAccountManager::onEvent;
		ami.cbData			= this;

		g_pF->fnCall(
			WTW_ACM_INTERFACE_ADD, ami, NULL);
	};
	
	CAccountManager::~CAccountManager()
	{
		g_pF->fnCall(WTW_ACM_INTERFACE_REMOVE, m_netClass, NULL);
	};

	WTW_PTR CAccountManager::addAccount(
		int netId,
		const wchar_t * accountName, const wchar_t * accountInfo,
		DWORD flags, const wchar_t * iconId)
	{
		wtwACMAccount ai;

		ai.accountName	= accountName;
		ai.accountInfo	= accountInfo;
		ai.iconId		= iconId;

		ai.netClass		= m_netClass;
		ai.netId		= netId;

		ai.flags		= flags;

		return g_pF->fnCall(
			WTW_ACM_ACCOUNT_ADD,
			ai, NULL);
	};

	WTW_PTR CAccountManager::onAccountDelete(const wtwACMAccount *pAccountInfo) 
	{
		return S_OK;
	};

	WTW_PTR CAccountManager::onAccountInfo(const wtwACMAccount *pAccountInfo) 
	{
		return S_OK;
	};

	WTW_PTR CAccountManager::onAccountEdit(const wtwACMAccount *pAccountInfo) 
	{
		return S_OK;
	}

	WTW_PTR CAccountManager::onEvent(WTW_PARAM wP, WTW_PARAM lP, void *od)
	{
		CAccountManager *pMan = reinterpret_cast<CAccountManager*>(wP);
		wtwACMEvent *pEvent = reinterpret_cast<wtwACMEvent*>(wP);

		switch (pEvent->event)
		{
		case ACM_EVENT_LIST:
			{
				return pMan->onAccountList();
			}
		case ACM_EVENT_ADD:
			{
				return pMan->onAccountAdd();
			}
		case ACM_EVENT_REMOVE:
			{
				return pMan->onAccountDelete(reinterpret_cast<const wtwACMAccount*>(lP));
			}
		case ACM_EVENT_INFO:
			{
				return pMan->onAccountInfo(reinterpret_cast<const wtwACMAccount*>(lP));
			}
		case ACM_EVENT_EDIT:
			{
				return pMan->onAccountEdit(reinterpret_cast<const wtwACMAccount*>(lP));
			}
		}

		return S_OK;
	}
};