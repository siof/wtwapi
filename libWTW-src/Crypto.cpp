/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/Crypto.h>
#include <wtwProtocol.h>

#pragma warning (disable: 4244) // possible loss of data
#pragma warning (disable: 4267) // possible loss of data

namespace wtw
{
	CCryptoHash::CCryptoHash(int hashType)
	{
		m_hashType = hashType;
		m_ctx = NULL;
	};

	CCryptoHash::~CCryptoHash()
	{
	};

	WTW_PTR CCryptoHash::init()
	{
		return wtw::g_pF->fnCall(WTW_CRYPTO_HASH_INIT, m_hashType, (WTW_PARAM)&m_ctx);
	}

	WTW_PTR CCryptoHash::update(const void * data, int dataLen)
	{
		if (!m_ctx)
			return E_FAIL;

		wtwRawDataDef rd;

		rd.pData	= (const char*)data;
		rd.pDataLen	= dataLen;

		return wtw::g_pF->fnCall(
			WTW_CRYPTO_HASH_UPDATE, (WTW_PARAM)m_ctx, rd);
	}

	WTW_PTR CCryptoHash::finish(char * hash, int hashLen)
	{
		wtwHashData hd;

		hd.hash			= hash;
		hd.hashLen		= hashLen;

		hd.flags		= WTW_CRYPTO_HASH_FLAG_OUT_BINARY;

		WTW_PTR r = g_pF->fnCall(
			WTW_CRYPTO_HASH_FINISH, (WTW_PARAM)m_ctx, hd);
		
		if (r == S_OK)
		{
			m_ctx = NULL;
		}
		
		return r;
	}

	WTW_PTR CCryptoHash::finish(wtw::CString & hash)
	{
		wchar_t h[128] = {0};
		wtwHashData hd;

		hd.hash			= h;
		hd.hashLen		= 128;
		hd.flags		= WTW_CRYPTO_HASH_FLAG_OUT_WCHAR;

		WTW_PTR r = g_pF->fnCall(
			WTW_CRYPTO_HASH_FINISH, (WTW_PARAM)m_ctx, hd);
		
		if (r == S_OK)
		{
			hash = h;
			m_ctx = NULL;
		}

		return r;
	}

	WTW_PTR CCryptoHash::hash(const void * data, int dataLen, wtw::CString & ret)
	{
		wchar_t h[128] = {0};
		wtwHashData hd;

		hd.pDataToHash	= data;
		hd.nDataToHash	= dataLen;
		hd.hash			= h;
		hd.hashLen		= 128;

		hd.hashType		= m_hashType;
		hd.flags		= WTW_CRYPTO_HASH_FLAG_OUT_WCHAR;

		WTW_PTR r = g_pF->fnCall(
			WTW_CRYPTO_HASH_SIMPLE, hd, NULL);

		ret = h;
		return r;
	}		

	WTW_PTR CCryptoHash::hash(const void * data, int dataLen, char * hash, int hashLen)
	{
		wtwHashData hd;

		hd.pDataToHash	= data;
		hd.nDataToHash	= dataLen;
		hd.hash			= hash;
		hd.hashLen		= hashLen;

		hd.hashType		= m_hashType;
		hd.flags		= WTW_CRYPTO_HASH_FLAG_OUT_BINARY;

		return g_pF->fnCall(
			WTW_CRYPTO_HASH_SIMPLE, hd, NULL);
	}		

	CCryptoBlockAlgoEBC::CCryptoBlockAlgoEBC()
	{
	};

	CCryptoBlockAlgoEBC::~CCryptoBlockAlgoEBC()
	{
	};

	CCryptoBlockAlgoCBC::CCryptoBlockAlgoCBC()
	{
	};
	
	CCryptoBlockAlgoCBC::~CCryptoBlockAlgoCBC()
	{
	};

	CCryptoAES::CCryptoAES(const wchar_t *passPhrase, int b256Bit)
	{
		m_keyEnc = m_keyDec = NULL;

		char key[64];

		wtwAESKeyInfoEx ki;

		ki.key		= (const BYTE*)key;

		CCryptoHash *pHash;

		if (b256Bit)
		{
			pHash = new CCryptoHash(WTW_CRYPTO_HASH_TYPE_SHA256);
			ki.keySize	= 256;
		}
		else
		{
			pHash = new CCryptoHash(WTW_CRYPTO_HASH_TYPE_MD5);
			ki.keySize	= 128;
		}

		if (!pHash)
			return;

		pHash->hash((const char*)passPhrase, wcslen(passPhrase) * sizeof(wchar_t), key, 16);

		ki.flags = WTW_CRYPTO_AES_KEY_FLAG_ENCRYPTION;
		g_pF->fnCall(
			WTW_CRYPTO_AES_EXPAND_KEY_EX, ki, NULL);

		m_keyEnc = ki.expandedKeyPtr;

		ki.flags = WTW_CRYPTO_AES_KEY_FLAG_DECRYPTION;
		g_pF->fnCall(
			WTW_CRYPTO_AES_EXPAND_KEY_EX, ki, NULL);

		m_keyDec = ki.expandedKeyPtr;

		delete pHash;
	};

	CCryptoAES::CCryptoAES(int keySizeInBits, void * pKey)
	{
		m_keyEnc = m_keyDec = NULL;
		wtwAESKeyInfoEx ki;

		ki.key		= (const BYTE*)pKey;
		ki.keySize	= keySizeInBits;

		ki.flags = WTW_CRYPTO_AES_KEY_FLAG_ENCRYPTION;
		g_pF->fnCall(
			WTW_CRYPTO_AES_EXPAND_KEY_EX, ki, NULL);

		m_keyEnc = ki.expandedKeyPtr;

		ki.flags = WTW_CRYPTO_AES_KEY_FLAG_DECRYPTION;
		g_pF->fnCall(
			WTW_CRYPTO_AES_EXPAND_KEY_EX, ki, NULL);

		m_keyDec = ki.expandedKeyPtr;
	};

	CCryptoAES::~CCryptoAES()
	{
		g_pF->fnCall(
			WTW_CRYPTO_AES_FREE_KEY, reinterpret_cast<WTW_PARAM>(m_keyDec), NULL);
		g_pF->fnCall(
			WTW_CRYPTO_AES_FREE_KEY, reinterpret_cast<WTW_PARAM>(m_keyEnc), NULL);
	};

	int CCryptoAES::getBlockLength()
	{
		return 16;
	};

	HRESULT CCryptoAES::blockEncrypt(void * dataBlock, int dataBlockLen)
	{
		if (!m_keyEnc)
			return E_FAIL;

		wtwCryptoData cd;

		cd.expandedKey	= m_keyEnc;
		cd.flags		= WTW_CRYPTO_AES_FLAG_MODE_ECB;
		cd.nData		= dataBlockLen;
		cd.pData		= dataBlock;

		return g_pF->fnCall(
			WTW_CRYPTO_AES_ENCRYPT, cd, NULL);
	};

	HRESULT CCryptoAES::blockDecrypt(void * dataBlock, int dataBlockLen)
	{
		if (!m_keyDec)
			return E_FAIL;

		wtwCryptoData cd;

		cd.expandedKey	= m_keyDec;
		cd.flags		= WTW_CRYPTO_AES_FLAG_MODE_ECB;
		cd.nData		= dataBlockLen;
		cd.pData		= dataBlock;

		return g_pF->fnCall(
			WTW_CRYPTO_AES_DECRYPT, cd, NULL);
	};

	HRESULT CCryptoAES::blockEncrypt(void * dataBlock, int dataBlockLen, void *ivec)
	{
		if (!m_keyEnc)
			return E_FAIL;

		wtwCryptoData cd;

		cd.expandedKey	= m_keyEnc;
		cd.flags		= WTW_CRYPTO_AES_FLAG_MODE_CBC;
		cd.nData		= dataBlockLen;
		cd.pData		= dataBlock;
		cd.ivec			= ivec;

		return g_pF->fnCall(
			WTW_CRYPTO_AES_ENCRYPT, cd, NULL);
	};

	HRESULT CCryptoAES::blockDecrypt(void * dataBlock, int dataBlockLen, void *ivec)
	{
		if (!m_keyDec)
			return E_FAIL;

		wtwCryptoData cd;

		cd.expandedKey	= m_keyDec;
		cd.flags		= WTW_CRYPTO_AES_FLAG_MODE_CBC;
		cd.nData		= dataBlockLen;
		cd.pData		= dataBlock;
		cd.ivec			= ivec;

		return g_pF->fnCall(
			WTW_CRYPTO_AES_DECRYPT, cd, NULL);
	};
};