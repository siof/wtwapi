#include "stdafx.h"
#include <cpp/Directory.h>
#include <math.h>

#pragma warning(disable: 4244)	// possible loss of data
#pragma warning(disable: 4267)  // possible loss of data

wtw::CDirectory::CDirectory()
{
}

wtw::CDirectory::~CDirectory()
{
}

QWORD wtw::CDirectory::getDirectorySize(const wchar_t *path, int bRecursive, const wchar_t *filter)
{
	QWORD fs = 0;
	wchar_t *fullPath = new wchar_t[32 * 1024]();
	
	wcscat(fullPath, L"\\\\?\\");
	wcscat(fullPath, path);
	wcscat(fullPath, L"\\");
	wcscat(fullPath, filter);

	WIN32_FIND_DATA wfd = {0};
	HANDLE hFind = ::FindFirstFile(
		fullPath, &wfd);

	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			if (wcscmp(L".", wfd.cFileName) &&
				wcscmp(L"..", wfd.cFileName))
			{
				if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (bRecursive)
					{
						wcscat(fullPath, path);
						wcscat(fullPath, L"\\");
						wcscat(fullPath, wfd.cFileName);

						fs += getDirectorySize(fullPath, 1);
					}
				}
				else
				{
					fs += ((QWORD)wfd.nFileSizeLow) | ((QWORD)wfd.nFileSizeHigh << 32);
				}
			}
		}
		while(::FindNextFile(hFind, &wfd));

		::FindClose(hFind);
	}
	delete [] fullPath;
	return fs;
}

QWORD wtw::CDirectory::deleteDirectory(const wchar_t *path, int bRecursive, const wchar_t *filter)
{
	QWORD fs = 0;
	wchar_t *fullPath = new wchar_t[32 * 1024]();
	
	wcscpy(fullPath, L"\\\\?\\");
	wcscat(fullPath, path);
	wcscat(fullPath, L"\\");
	wcscat(fullPath, filter);

	WIN32_FIND_DATA wfd = {0};
	HANDLE hFind = ::FindFirstFile(
		fullPath, &wfd);

	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			if (wcscmp(L".", wfd.cFileName) &&
				wcscmp(L"..", wfd.cFileName))
			{
				if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (bRecursive)
					{
						wcscpy(fullPath, path);
						wcscat(fullPath, L"\\");
						wcscat(fullPath, wfd.cFileName);

						fs += deleteDirectory(fullPath, 1);
					}
				}
				else
				{
					wcscpy(fullPath, path);
					wcscat(fullPath, L"\\");
					wcscat(fullPath, wfd.cFileName);

					if (::DeleteFile(fullPath))
					{
						fs += ((QWORD)wfd.nFileSizeLow) | ((QWORD)wfd.nFileSizeHigh << 32);
					}
				}
			}
		}
		while(::FindNextFile(hFind, &wfd));

		::FindClose(hFind);
		::DeleteFile(path);
	}
	delete [] fullPath;
	return fs;
}
