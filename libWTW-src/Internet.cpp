/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Internet.h>
#include <cpp/Globals.h>
#include <cpp/Conv.h>
#include <wtwNetSett.h>
#include <curl/curl.h>

#pragma warning (disable: 4244) // possible loss of data

namespace wtw 
{
	HRESULT ctoh(CURLcode cc)
	{
		if (cc != CURLE_OK)
			return MAKE_SCODE(SEVERITY_ERROR, FACILITY_INTERNET, cc);
		else
			return S_OK;
		/*
		switch (cc)
		{
		case CURLE_OK:
			return S_OK;
		case CURLE_REMOTE_ACCESS_DENIED:
			return E_ACCESSDENIED;
		case CURLE_AGAIN:
			return E_NOT_VALID_STATE;
		case CURLE_UNSUPPORTED_PROTOCOL:
		case CURLE_NOT_BUILT_IN:
		case CURLE_FUNCTION_NOT_FOUND:
			return E_NOINTERFACE;
		case CURLE_FAILED_INIT:
		case CURLE_FTP_CANT_GET_HOST:
		case CURLE_COULDNT_RESOLVE_PROXY:
		case CURLE_COULDNT_RESOLVE_HOST:
		case CURLE_COULDNT_CONNECT:
		case CURLE_INTERFACE_FAILED:
			return E_FAIL;
		case CURLE_URL_MALFORMAT:
			return E_INVALIDARG;
		case CURLE_OUT_OF_MEMORY:
			return E_OUTOFMEMORY;
		case CURLE_LOGIN_DENIED:
			return MAKE_SCODE(SEVERITY_ERROR, FACILITY_HTTP, 5);
		case CURLE_FTP_WEIRD_SERVER_REPLY:
		case CURLE_FTP_ACCEPT_FAILED:
		case CURLE_FTP_WEIRD_PASS_REPLY:
		case CURLE_FTP_ACCEPT_TIMEOUT:
		case CURLE_FTP_WEIRD_PASV_REPLY:
		case CURLE_FTP_WEIRD_227_FORMAT:
		case CURLE_FTP_COULDNT_SET_TYPE:
		case CURLE_PARTIAL_FILE:
		case CURLE_FTP_COULDNT_RETR_FILE:
		case CURLE_QUOTE_ERROR:
		case CURLE_HTTP_RETURNED_ERROR:
		case CURLE_WRITE_ERROR:
		case CURLE_UPLOAD_FAILED:
		case CURLE_READ_ERROR:
		case CURLE_OPERATION_TIMEDOUT:
		case CURLE_FTP_PORT_FAILED:
		case CURLE_FTP_COULDNT_USE_REST:
		case CURLE_RANGE_ERROR:
		case CURLE_HTTP_POST_ERROR:
		case CURLE_SSL_CONNECT_ERROR:
		case CURLE_BAD_DOWNLOAD_RESUME:
		case CURLE_FILE_COULDNT_READ_FILE:
		case CURLE_LDAP_CANNOT_BIND:
		case CURLE_LDAP_SEARCH_FAILED:
		case CURLE_ABORTED_BY_CALLBACK:
		case CURLE_BAD_FUNCTION_ARGUMENT:
		case CURLE_TOO_MANY_REDIRECTS :
		case CURLE_UNKNOWN_OPTION:
		case CURLE_TELNET_OPTION_SYNTAX :
		case CURLE_PEER_FAILED_VERIFICATION:
		case CURLE_GOT_NOTHING:
		case CURLE_SSL_ENGINE_NOTFOUND:
		case CURLE_SSL_ENGINE_SETFAILED:
		case CURLE_SEND_ERROR:
		case CURLE_RECV_ERROR:
		case CURLE_SSL_CERTPROBLEM:
		case CURLE_SSL_CIPHER:
		case CURLE_SSL_CACERT:
		case CURLE_BAD_CONTENT_ENCODING:
		case CURLE_LDAP_INVALID_URL:
		case CURLE_FILESIZE_EXCEEDED:
		case CURLE_USE_SSL_FAILED:
		case CURLE_SEND_FAIL_REWIND:
		case CURLE_SSL_ENGINE_INITFAILED:
		case CURLE_TFTP_NOTFOUND:
		case CURLE_TFTP_PERM:
		case CURLE_REMOTE_DISK_FULL:
		case CURLE_TFTP_ILLEGAL:
		case CURLE_TFTP_UNKNOWNID:
		case CURLE_REMOTE_FILE_EXISTS:
		case CURLE_TFTP_NOSUCHUSER:
		case CURLE_CONV_FAILED:
		case CURLE_CONV_REQD:
		case CURLE_SSL_CACERT_BADFILE:
		case CURLE_REMOTE_FILE_NOT_FOUND:
		case CURLE_SSL_SHUTDOWN_FAILED:
		case CURLE_SSL_CRL_BADFILE:
		case CURLE_SSL_ISSUER_ERROR:
		case CURLE_FTP_PRET_FAILED:
		case CURLE_RTSP_CSEQ_ERROR:
		case CURLE_RTSP_SESSION_ERROR:
		case CURLE_FTP_BAD_FILE_LIST:
		case CURLE_CHUNK_FAILED:
		case CURLE_NO_CONNECTION_AVAILABLE: 
		default:
			return E_UNEXPECTED;
		}
		*/
	}

	CInternet::CInternet()
	{
		m_hNet = NULL;
	};

	CInternet::~CInternet()
	{
		if (m_hNet)
			close();
	};

	HRESULT CInternet::open(DWORD flags, const wchar_t *userAgent)
	{
		wtwNetSettings ns;
		wchar_t proxy[255] = {0};

		wtw::g_pF->fnCall(
			WTW_NETLIB_GET_DEFAULT_SETTINGS, ns, NULL);

		m_hNet = curl_easy_init();

		if (!m_hNet)
			return E_OUTOFMEMORY;

		char *tmp = wtw::CConv::wtou(userAgent);
		curl_easy_setopt(m_hNet, CURLOPT_USERAGENT, tmp);
		wtw::CConv::release(tmp);

		switch (ns.proxyType)
		{
		case WTW_NETLIB_PROXYTYPE_HTTP11:
		case WTW_NETLIB_PROXYTYPE_SOCKS5:
			swprintf(proxy, 255, L"%s:%d", ns.proxyHost, ns.proxyPort);
			tmp = wtw::CConv::wtou(proxy);
			curl_easy_setopt(m_hNet, CURLOPT_PROXY, tmp);
			wtw::CConv::release(tmp);

			curl_easy_setopt(m_hNet, CURLOPT_PROXYTYPE, (ns.proxyType == WTW_NETLIB_PROXYTYPE_SOCKS5) ? CURLPROXY_SOCKS5 : CURLPROXY_HTTP);
			break;
		case WTW_NETLIB_PROXYTYPE_NONE:
		default:
			break;
		}

		int bReturnProblem = 0;
		if (ns.proxyAuth && 
			(ns.proxyType == WTW_NETLIB_PROXYTYPE_HTTP11 || ns.proxyType == WTW_NETLIB_PROXYTYPE_SOCKS5))
		{
			curl_easy_setopt(m_hNet, CURLOPT_PROXYAUTH, 1);

			tmp = wtw::CConv::wtou(ns.proxyUser);
			curl_easy_setopt(m_hNet, CURLOPT_PROXYUSERNAME, tmp);
			wtw::CConv::release(tmp);

			tmp = wtw::CConv::wtou(ns.proxyPass);
			curl_easy_setopt(m_hNet, CURLOPT_PROXYPASSWORD, tmp);
			wtw::CConv::release(tmp);
		}

		return (m_hNet != NULL) ? ((bReturnProblem == 0) ? S_OK : S_FALSE) : E_FAIL;
	};

	HRESULT CInternet::open(const wchar_t * proxyHost, int proxyPort, DWORD flags, const wchar_t *userAgent)
	{
		int proxyType;
		wchar_t proxy[255] = {0};
		m_hNet = curl_easy_init();

		if (!m_hNet)
			return E_OUTOFMEMORY;

		char *tmp = wtw::CConv::wtou(userAgent);
		curl_easy_setopt(m_hNet, CURLOPT_USERAGENT, tmp);
		wtw::CConv::release(tmp);

		if (proxyHost)
		{
			proxyType = WTW_NETLIB_PROXYTYPE_HTTP11;
		}
		else
		{
			proxyType = WTW_NETLIB_PROXYTYPE_NONE;
		}

		switch (proxyType)
		{
		case WTW_NETLIB_PROXYTYPE_HTTP11:
		case WTW_NETLIB_PROXYTYPE_SOCKS5:
			swprintf(proxy, 255, L"%s:%d", proxyHost, proxyPort);
			tmp = wtw::CConv::wtou(proxy);
			curl_easy_setopt(m_hNet, CURLOPT_PROXY, tmp);
			wtw::CConv::release(tmp);

			curl_easy_setopt(m_hNet, CURLOPT_PROXYTYPE, (proxyType == WTW_NETLIB_PROXYTYPE_SOCKS5) ? CURLPROXY_SOCKS5 : CURLPROXY_HTTP);
			break;
		case WTW_NETLIB_PROXYTYPE_NONE:
		default:
			break;
		}

		return (m_hNet != NULL) ? S_OK : E_FAIL;
	};

	HRESULT CInternet::close()
	{
		if (m_hNet)
		{
			//::InternetCloseHandle(m_hNet);
			curl_easy_cleanup(reinterpret_cast<CURL*>(m_hNet));
			m_hNet = NULL;
			return S_OK;
		}
		return S_FALSE;
	};

	HRESULT CInternet::attach(void* hNet)
	{
		close();
		m_hNet = hNet;
		return S_OK;
	};

	void* CInternet::detach()
	{
		void* hNet = m_hNet;
		m_hNet = NULL;
		return hNet;
	};

	HRESULT CInternet::isOpen()
	{
		return (m_hNet != NULL) ? S_OK : S_FALSE;
	};
};