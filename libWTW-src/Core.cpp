/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/core.h>
#include <wtwProtocol.h>
#include <wtwOther.h>
#include <wtwAbout.h>
#include <wtwLog.h>
#include <wtwPluginSettings.h>

namespace wtw
{

	CCore::CCore(WTWFUNCTIONS *pF)
	{
		fnCall		= pF->fnCall;
		fnCreate	= pF->fnCreate;
		fnDestroy	= pF->fnDestroy;
		fnDestroyAll= pF->fnDestroyAll;
		fnExists	= pF->fnExists;
		fnGet		= pF->fnGet;

		evCreate	= pF->evCreate;
		evDestroy	= pF->evDestroy;
		evHook		= pF->evHook;
		evNotify	= pF->evNotify;
		evUnhook	= pF->evUnhook;
	}

	CCore::~CCore()
	{
		ASSERT(fnDestroyAll);
		fnDestroyAll(NULL);
	}

	HWND CCore::getMainHWND()
	{
		HWND hWnd = NULL;
		fnCall(WTW_GET_MAIN_HWND_EX, (WTW_PTR)&hWnd, 0);
		return hWnd;
	}

	void CCore::log(const wchar_t *mdl, 
				const wchar_t *msg, 
				int lvl
				)
	{

		wtwLogDef p;

		p.logLevel = lvl;
		p.pMessage = msg;
		p.pModName = mdl;

		fnCall(
			WTW_LOG_FUNCTION, 
			p, 
			NULL
			);
	}

	void CCore::logCritical(const wchar_t *msg, 
						const wchar_t * graphId
					)
	{
		wtwLogDef p;
		initStruct(p);

		p.logLevel	= -2;
		p.pMessage	= msg;
		p.pModName	= NULL;
		p.graphId	= graphId;

		fnCall(
			WTW_LOG_FUNCTION, 
			reinterpret_cast<WTW_PARAM>(&p), 
			NULL
			);
	}

	void CCore::logFormatted(int lvl, 
					  const wchar_t* mdl, 
					  const wchar_t* format, ...
					  )
	{
		va_list args; 
		va_start(args, format); 
		wchar_t buf[2048] = { 0 }; 
		_vsnwprintf(buf, sizeof(buf) / sizeof(wchar_t), format, args); 
		log(mdl, buf, lvl); 
		va_end(args);
	} 

	const wchar_t *CCore::getProfileDir()
	{
		const wchar_t *ret = L"";
		fnCall(WTW_GET_PROFILE_DIR_PTR_EX, (WTW_PARAM)&ret, NULL);
		return ret;
	}

	const wchar_t *CCore::getProfileName()
	{
		const wchar_t *ret = L"";
		fnCall(WTW_GET_PROFILE_NAME_PTR_EX, (WTW_PARAM)&ret, NULL);
		return ret;
	}

	wchar_t * CCore::getDirectory(int dirId)
	{
		wtwDirectoryInfo di;

		di.dirType			= dirId;
		di.bi.bufferSize	= MAX_PATH;
		di.bi.pBuffer		= new wchar_t[MAX_PATH]();

		di.flags			= WTW_DIRECTORY_FLAG_FULLPATH;

		fnCall(WTW_GET_DIRECTORY_LOCATION, di, NULL);

		return di.bi.pBuffer;
	}

	HANDLE CCore::installProtocolFunction(
					const wchar_t * netClass,
					int				netId,
					const wchar_t * funcName,
					WTWFUNCTION		funcAddr,
					void *			cbData
					)
	{
		return wtwInstProtoFunc(this, netClass, netId, funcName, funcAddr, cbData);
	}

};