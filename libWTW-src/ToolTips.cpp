/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Globals.h>
#include <cpp/ToolTips.h>

namespace wtw {

	CToolTips::CToolTips()
	{
		m_hWnd = NULL;
	};

	CToolTips::~CToolTips()
	{
	};

	int CToolTips::create(HWND hParentWindow)
	{
		if (m_hWnd)
			return 0;

		m_hWnd = hParentWindow;

		g_pF->fnCall(WTW_TTIP_INITIALIZE, (WTW_PARAM)m_hWnd, 0);
		return 1;
	}

	int CToolTips::destroy()
	{
		if (!m_hWnd)
			return 0;

		g_pF->fnCall(WTW_TTIP_TERMINATE, (WTW_PARAM)m_hWnd, 0);
		m_hWnd = NULL;
		return 1;
	}

	int CToolTips::addTool(DWORD ctrlId, const wchar_t *szSimpleHtmlText)
	{
		ASSERT(m_hWnd);
		if (!m_hWnd)
			return 0;

		wtwToolTipAdd ti;
		initStruct(ti);

		ti.hMainWnd		= m_hWnd;
		ti.hTipWindow	= ::GetDlgItem(m_hWnd, ctrlId);
		ti.tipText		= szSimpleHtmlText;

		g_pF->fnCall(WTW_TTIP_ADD_TOOL, (WTW_PARAM)&ti, 0);
		return 1;
	};

	int CToolTips::addTool(HWND hCtrl, const wchar_t *szSimpleHtmlText)
	{
		ASSERT(m_hWnd);
		if (!m_hWnd)
			return 0;

		wtwToolTipAdd ti;
		initStruct(ti);

		ti.hMainWnd		= m_hWnd;
		ti.hTipWindow	= hCtrl;
		ti.tipText		= szSimpleHtmlText;

		g_pF->fnCall(WTW_TTIP_ADD_TOOL, (WTW_PARAM)&ti, 0);
		return 1;
	};
};