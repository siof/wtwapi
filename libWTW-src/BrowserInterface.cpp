/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/BrowserInterface.h>

namespace wtw {

	CBrowserObject::CBrowserObject()
	{
		m_objectData = NULL;
		m_objectType = 0;
	}

	CBrowserObject::~CBrowserObject()
	{
	}

	CBrowserEventSink::CBrowserEventSink()
	{
	}

	CBrowserEventSink::~CBrowserEventSink()
	{
	}

	WTW_PTR CBrowserEventSink::onDocumentCompleted()
	{
		return E_NOTIMPL;
	}

	WTW_PTR CBrowserEventSink::onRButtonUp(
		const POINT &pt,
		CBrowserObject *pObjects,
		DWORD objectCount
		)
	{
		return E_NOTIMPL;
	};

	WTW_PTR CBrowserEventSink::onLinkNavigate(
		const POINT &pt,
		const wchar_t *pLinkTarget,
		CBrowserObject *pObjects,
		DWORD objectCount
		)
	{
		return E_NOTIMPL;
	};
	
	WTW_PTR CBrowserEventSink::onBeforeNavigate(
		const wchar_t *pLinkTarget,
		const wchar_t *pTarget,
		DWORD flags
		)
	{
		return E_NOTIMPL;
	};

	WTW_PTR CBrowserEventSink::onLButtonUp(
		const POINT &pt,
		CBrowserObject *pObjects,
		DWORD objectCount
		)
	{
		return E_NOTIMPL;
	};

	WTW_PTR CBrowserEventSink::onMButtonUp(
		const POINT &pt,
		CBrowserObject *pObjects,
		DWORD objectCount
		)
	{
		return E_NOTIMPL;
	};

	WTW_PTR CBrowserEventSink::onLDblClick(
		const POINT &pt,
		CBrowserObject *pObjects,
		DWORD objectCount
		)
	{
		return E_NOTIMPL;
	};

	CBrowserInterface::CBrowserInterface()
	{
		m_pSink = NULL;
	}

	CBrowserInterface::~CBrowserInterface()
	{
		m_pSink = NULL;
	}

};