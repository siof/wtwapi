/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Buffer.h>

namespace wtw
{

	CBuffer::CBuffer(void)
	{
		m_pBuffer		= NULL;
		m_iAllocSize	= 0;
		m_iDataSize		= 0;	
	}

	CBuffer::~CBuffer(void)
	{
		disposeBuffers();
	}

	BYTE *	CBuffer::getBuffer() const
	{
		return m_pBuffer;
	}

	BYTE *	CBuffer::getBuffer(DWORD &dataSize) const
	{
		dataSize = m_iDataSize;
		return m_pBuffer;
	}

	int		CBuffer::replaceBuffer(BYTE *pData, DWORD dataSize)
	{
		if (!pData)
		{
			disposeBuffers();
			return 0;
		}

		if (m_iAllocSize >= (dataSize + 2))
		{
			memmove(m_pBuffer, pData, dataSize);
			m_iDataSize = dataSize;
			memset(m_pBuffer + m_iDataSize, 0, m_iAllocSize - m_iDataSize);
			return m_iDataSize;
		}

		DWORD a = 0;
		BYTE *tmp = alignedRealloc(NULL, a, dataSize, 0);
		memcpy(tmp, pData, dataSize);
		m_iAllocSize = a;
		m_iDataSize = dataSize;

		free(m_pBuffer);
		m_pBuffer = tmp;

		return m_iDataSize;
	}

	int		CBuffer::push(BYTE *pData, DWORD dataSize)
	{
		m_pBuffer	= alignedRealloc(m_pBuffer, m_iAllocSize, m_iDataSize + dataSize, m_iDataSize);
		memmove(m_pBuffer + m_iDataSize, pData, dataSize);
		m_iDataSize += dataSize;
		return m_iDataSize;
	}

	int		CBuffer::popHead(DWORD size)
	{
		if (m_iDataSize <= size)
			disposeBuffers();
		else
		{
			m_iDataSize -= size;
			memmove(m_pBuffer, m_pBuffer + size, m_iDataSize);
			memset(m_pBuffer + m_iDataSize, 0, m_iAllocSize - m_iDataSize); 
		}
			 
		return m_iDataSize;
	}

	void	CBuffer::disposeBuffers()
	{
		if (!m_pBuffer)
			return;

		free(m_pBuffer);
		m_iDataSize = 0;
		m_iAllocSize = 0;
		m_pBuffer = NULL;
	}

	void	CBuffer::resize(DWORD size)
	{
		m_pBuffer = alignedRealloc(m_pBuffer, m_iAllocSize, size, m_iDataSize);
	}

	BYTE * CBuffer::alignedRealloc(void *p, DWORD & allocated, DWORD size, DWORD used)
	{
		// for C-(W)-String compatibility, always allocate 2 bytes more for null charter 
		size += 2;
		const int align = 256;

		if (allocated >= size)
			return (BYTE*)p;

		allocated = (align - (size % align)) + size;
		BYTE* ret = (BYTE*)realloc(p, allocated);	// expand/alloc buffer
		memset(ret + used, 0, allocated - used);	// clear unused space
		return ret;
	}

	BYTE * CBuffer::detach(int & dataLen, int * realLength)
	{
		BYTE *pB = m_pBuffer;
		dataLen = m_iDataSize;

		if (realLength)
			*realLength = m_iAllocSize;

		m_iAllocSize	= 0;
		m_iDataSize		= 0;
		m_pBuffer		= NULL;

		return pB;
	}
};