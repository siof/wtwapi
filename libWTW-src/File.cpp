/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/File.h>
#include <cpp/Globals.h>

#pragma warning(disable: 4244)

namespace wtw 
{
	CFile::CFile()
	{
		g_pF->fnCall(WTW_FILE_CREATE_EX, (WTW_PARAM)(void**)&m_pFile, NULL);
	};

	CFile::~CFile()
	{
		if (m_pFile)
			g_pF->fnCall(WTW_FILE_DESTROY, (WTW_PARAM)m_pFile, NULL);

		m_pFile = NULL;
	};

	bool CFile::open(const wchar_t *filePath, int bRead, int bWrite, int bCreate, int bTruncate, HRESULT *pExtInfo, int bNoEnc)
	{
		wtwFileOpen o;

		o.bCreateifNotExists	= bCreate;
		o.bReadAccess			= bRead;
		o.bTruncate				= bTruncate;
		o.bWriteAccess			= bWrite;
		o.pFileObject			= m_pFile;
		o.filePath				= filePath;

		if (bNoEnc)
			o.flags |= WTW_FILE_OPEN_FLAG_NO_ENC;

		HRESULT ret = g_pF->fnCall(WTW_FILE_OPEN, o, NULL);

		if (pExtInfo)
			*pExtInfo = ret;

		return SUCCEEDED(ret) ? 1 : 0;
	};

	bool CFile::close()
	{
		return SUCCEEDED(g_pF->fnCall(WTW_FILE_CLOSE, (WTW_PARAM)m_pFile, NULL)) ? 1 : 0;
	};

	bool CFile::read(void *pData, DWORD nData, DWORD &bytesProcessed)
	{
		wtwFileData d;

		d.pFileObject		= m_pFile;
		d.nBufferLen		= nData;
		d.pBuffer			= pData;

		HRESULT ret = g_pF->fnCall(WTW_FILE_READ, d, NULL);

		bytesProcessed = d.nBytesProcessed;

		return SUCCEEDED(ret) ? 1 : 0;
	};

	bool CFile::write(void *pData, DWORD nData, DWORD &bytesProcessed)
	{
		wtwFileData d;

		d.pFileObject		= m_pFile;
		d.nBufferLen		= nData;
		d.pBuffer			= pData;

		HRESULT ret = g_pF->fnCall(WTW_FILE_WRITE, d, NULL);

		bytesProcessed = d.nBytesProcessed;

		return SUCCEEDED(ret) ? 1 : 0;
	};

	QWORD CFile::seek(long long offset, int from)
	{
		wtwFileSize s;

		s.pFileObject	= m_pFile;
		s.size			= offset;
		s.param			= from;

		HRESULT ret = g_pF->fnCall(WTW_FILE_SEEK, s, NULL);

		return s.size;
	};

	QWORD CFile::getLength()
	{
		wtwFileSize s;

		s.pFileObject	= m_pFile;

		HRESULT ret = g_pF->fnCall(WTW_FILE_GET_SIZE, s, NULL);

		return s.size;
	};

	QWORD CFile::seekToEnd()
	{
		return seek(0, WFS_END);
	};

	QWORD CFile::seekToBegin()
	{
		return seek(0, WFS_BEGIN);
	};

	bool CFile::encrypt()
	{
		return SUCCEEDED(g_pF->fnCall(WTW_FILE_ENCRYPT, (WTW_PARAM)m_pFile, NULL)) ? 1 : 0;
	};

	bool CFile::decrypt()
	{
		return SUCCEEDED(g_pF->fnCall(WTW_FILE_DECRYPT, (WTW_PARAM)m_pFile, NULL)) ? 1 : 0;
	};

	DWORD CFile::getSectorSize()
	{
		wtwFileSize s;

		s.pFileObject	= m_pFile;
		
		HRESULT ret = g_pF->fnCall(WTW_FILE_GET_SECTOR_SIZE, s, NULL);

		return s.size;
	};

	bool CFile::setSectorSize(DWORD size)
	{
		wtwFileSize s;

		s.pFileObject	= m_pFile;
		s.size			= size;
		
		return SUCCEEDED(g_pF->fnCall(WTW_FILE_GET_SECTOR_SIZE, s, NULL)) ? 1 : 0;
	};

	/////////////////////////////////////////////////////////////////////////////////////////

	QWORD CFile::getLength(const wchar_t *path)
	{
		WIN32_FILE_ATTRIBUTE_DATA wfad = {0};
		if (::GetFileAttributesEx(path, GetFileExInfoStandard, &wfad))
		{
			return (QWORD)wfad.nFileSizeLow | ((QWORD)wfad.nFileSizeHigh << 32); 
		}
		return WTW_INVALID_FILE_LENGTH;
	};

	DWORD CFile::getAttributes(const wchar_t *path)
	{
		WIN32_FILE_ATTRIBUTE_DATA wfad = {0};
		if (::GetFileAttributesEx(path, GetFileExInfoStandard, &wfad))
		{
			return wfad.dwFileAttributes; 
		}
		return WTW_INVALID_ATTRIBUTES;
	};

	bool CFile::fileExists(const wchar_t *path)
	{
		return (::GetFileAttributes(path) == INVALID_FILE_ATTRIBUTES) ? false : true;
	}

	bool CFile::fileDelete(const wchar_t *path)
	{
		return ::DeleteFile(path);
	}
};