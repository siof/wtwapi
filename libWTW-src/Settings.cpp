/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Globals.h>
#include <cpp/SettingsHelper.h>

#pragma warning(disable: 4244)

namespace wtw {

	CSettings::CSettings()
	{
		m_pS = NULL;
	}

	CSettings::CSettings(void *pSettings)
	{
		m_pS = pSettings;
	}

	int CSettings::attach(void *pSettings)
	{
		ASSERT(!m_pS);
		if (m_pS)
			return 0;

		m_pS = pSettings;

		return 1;
	}

	CSettings::~CSettings()
	{
	}

	void * CSettings::detach()
	{
		return m_pS;
	}

	void CSettings::destroy()
	{
		if (!m_pS)
			return;

		g_pF->fnCall(WTW_SETTINGS_DESTROY, (WTW_PARAM)m_pS, NULL);
		m_pS = NULL;
	}

	int CSettings::create(const wchar_t *pFileName)
	{
		ASSERT(!m_pS);
		if (m_pS)
			return 0;

		if (SUCCEEDED(g_pF->fnCall(WTW_SETTINGS_INIT_EX, 
			reinterpret_cast<WTW_PARAM>(pFileName), 
			(WTW_PARAM)&m_pS)))
			return 1;
		else
			return 0;
	}

	int CSettings::read()
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;

		return g_pF->fnCall(WTW_SETTINGS_READ,
			reinterpret_cast<WTW_PARAM>(m_pS), 
			NULL);
	}

	int CSettings::write()
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;

		return g_pF->fnCall(WTW_SETTINGS_WRITE, 
			reinterpret_cast<WTW_PARAM>(m_pS), 
			NULL);
	}

	int CSettings::getInt(const wchar_t *pKeyName, int defaultValue)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwGetInt(g_pF, m_pS, pKeyName, defaultValue);
	}

	LONGLONG CSettings::getInt64(const wchar_t *pKeyName, __int64 defaultValue)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwGetInt64(g_pF, m_pS, pKeyName, defaultValue);
	}

	int CSettings::setInt(const wchar_t *pKeyName, int value)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwSetInt(g_pF, m_pS, pKeyName, value);
	}

	int CSettings::setInt64(const wchar_t *pKeyName, __int64 value)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwSetInt64(g_pF, m_pS, pKeyName, value);
	}

	int CSettings::getString(const wchar_t *pKeyName, const wchar_t * defaultValue, wchar_t **pRetPtr)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwGetStr(g_pF, m_pS, pKeyName, defaultValue, pRetPtr);
	}

	int CSettings::setString(const wchar_t *pKeyName, const wchar_t * pRetPtr)
	{
		ASSERT(m_pS);
		if (!m_pS)
			return -1;
		return wtwSetStr(g_pF, m_pS, pKeyName, pRetPtr);
	}

	CSettingsBinder::CSettingsBinder()
	{
		m_hMainWnd = NULL;
		m_bindings = new std::list<_binding>;
	};

	CSettingsBinder::CSettingsBinder(HWND hOptWindow)
	{
		m_hMainWnd = hOptWindow;
		m_bindings = new std::list<_binding>;
	};

	CSettingsBinder::~CSettingsBinder()
	{
		delete (std::list<_binding>*)m_bindings;
	};

	int CSettingsBinder::bindInt(
		const wchar_t *settingName, int defaultValue,
		HWND controlHwnd, DWORD controlType
		)
	{
		ASSERT(controlHwnd);
		CSettingsBinder::_binding g;

		g.name		= settingName;
		g.type		= SETTING_TYPE_INT;
		g.control	= controlType;
		g.hControl	= controlHwnd;
		g.def		= defaultValue;

		((std::list<_binding>*)m_bindings)->push_back(g);
		return 0;
	}

	int CSettingsBinder::bindInt(
		const wchar_t *settingName, int defaultValue,
		int controlId, DWORD controlType
		)
	{
		ASSERT(controlId > 0);
		ASSERT(m_hMainWnd);
		return bindInt(settingName, defaultValue,
			::GetDlgItem(m_hMainWnd, controlId), controlType);
	}

	int CSettingsBinder::bindString(
		const wchar_t *settingName, const wchar_t * defaultValue,
		int controlId, DWORD controlType
		)
	{
		ASSERT(controlId > 0);
		ASSERT(m_hMainWnd);
		return bindString(settingName, defaultValue,
			::GetDlgItem(m_hMainWnd, controlId), controlType);
	}

	int CSettingsBinder::bindString(
		const wchar_t *settingName, const wchar_t * defaultValue,
		HWND controlHwnd, DWORD controlType
		)
	{
		ASSERT(controlHwnd);
		CSettingsBinder::_binding g;

		g.name		= settingName;
		g.type		= SETTING_TYPE_STRING;
		g.control	= controlType;
		g.hControl	= controlHwnd;
		g.def		= (__int64)defaultValue;

		((std::list<_binding>*)m_bindings)->push_back(g);
		return 0;
	}

	int CSettingsBinder::settingsToControls(const CSettings &cs)
	{
		int c = 0;
		for(std::list<_binding>::iterator it = ((std::list<_binding>*)m_bindings)->begin();
			it != ((std::list<_binding>*)m_bindings)->end(); it++)
		{
			ASSERT(it->hControl);
			if (it->type == SETTING_TYPE_INT &&
				it->control == CONTROL_TYPE_CHECKBOX)
			{
				::SendMessage(it->hControl, BM_SETCHECK, 
					wtwGetInt(g_pF, cs.m_pS, it->name, it->def), 0);
				continue;
			}

			if (it->type == SETTING_TYPE_INT &&
				it->control == CONTROL_TYPE_COMBO_INDEX)
			{
				::SendMessage(it->hControl, CB_SETCURSEL, 
					wtwGetInt(g_pF, cs.m_pS, it->name, it->def), 0);
				continue;
			}

			if (it->type == SETTING_TYPE_STRING &&
				(it->control == CONTROL_TYPE_EDIT || it->control == CONTROL_TYPE_COMBO_EDIT))
			{
				wchar_t *ret;
				wtwGetStr(
					g_pF, cs.m_pS, 
					it->name, (const wchar_t *)it->def, &ret);
				::SetWindowTextW(it->hControl, ret);
				delete [] ret;
				continue;
			}

			if (it->type == SETTING_TYPE_INT &&
				(it->control == CONTROL_TYPE_EDIT || it->control == CONTROL_TYPE_COMBO_EDIT))
			{
				wchar_t ret[1024] = {0};
				wsprintf(ret, L"%d", wtwGetInt(
					g_pF, cs.m_pS, 
					it->name, it->def));
				::SetWindowTextW(it->hControl, ret);
				continue;
			}

			ASSERT(0);
		}
		return c;
	};

	int CSettingsBinder::controlsToSettings(const CSettings &cs)
	{
		int c = 0;
		for(std::list<_binding>::iterator it = ((std::list<_binding>*)m_bindings)->begin();
			it != ((std::list<_binding>*)m_bindings)->end(); it++)
		{
			ASSERT(it->hControl);
			if (it->type == SETTING_TYPE_INT &&
				it->control == CONTROL_TYPE_CHECKBOX)
			{
				wtwSetInt(
					g_pF, cs.m_pS, it->name, 
					::SendMessage(it->hControl, BM_GETCHECK, 0, 0));
				continue;
			}
			
			if (it->type == SETTING_TYPE_INT &&
				it->control == CONTROL_TYPE_COMBO_INDEX)
			{
				wtwSetInt(
					g_pF, cs.m_pS, it->name, 
					::SendMessage(it->hControl, CB_GETCURSEL, 0, 0));
				continue;
			}
			
			if (it->type == SETTING_TYPE_STRING &&
				(it->control == CONTROL_TYPE_EDIT || it->control == CONTROL_TYPE_COMBO_EDIT))
			{
				int l = ::GetWindowTextLength(it->hControl) + 16;
				wchar_t *text = new wchar_t[l]();
				::GetWindowTextW(it->hControl, text, l - 1);
				wtwSetStr(g_pF, cs.m_pS, it->name, text);
				delete [] text;
				continue;
			}

			if (it->type == SETTING_TYPE_INT &&
				(it->control == CONTROL_TYPE_EDIT || it->control == CONTROL_TYPE_COMBO_EDIT))
			{
				int l = ::GetWindowTextLength(it->hControl) + 16;
				wchar_t *text = new wchar_t[l]();
				::GetWindowTextW(it->hControl, text, l - 1);
				wtwSetInt(g_pF, cs.m_pS, it->name, wcstol(text, 0, 0));
				delete [] text;
				continue;
			}

			ASSERT(0);
		}
		return c;
	};

	void CSettingsBinder::setMainHwnd(HWND hOptWindow)
	{
		m_hMainWnd = hOptWindow;
	}

};