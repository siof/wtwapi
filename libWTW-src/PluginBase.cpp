/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/core.h>
#include <cpp/Plugin.h>
#include <cpp/Globals.h>

namespace wtw
{
	CPluginBase::CPluginBase()
	{
	}

	int CPluginBase::load(DWORD callReason)
	{
		return S_OK;
	}

	int CPluginBase::unload(DWORD callReason)
	{
		cleanupEvents();
		cleanupFunctions();
	
		return S_OK;
	}

	HRESULT CPluginBase::addFunction(const wchar_t *name, WTWFUNCTION fn, void *ownerData)
	{
		HANDLE hFunc = g_pF->fnCreate(name, fn, ownerData);

		if (!hFunc)
			return E_FAIL;

		m_hRegFuncs.push_back(hFunc);

		return S_OK;
	}

	HRESULT CPluginBase::hookEvent(const wchar_t *name, WTWHOOK cb, void *cbData)
	{
		HANDLE hEvent = g_pF->evHook(name, cb, cbData);

		if (!hEvent)
			return E_FAIL;

		m_hHookedEvs.push_back(hEvent);

		return S_OK;
	}

	HRESULT CPluginBase::cleanupFunctions()
	{
		for (std::deque<HANDLE>::iterator it = m_hRegFuncs.begin(); it != m_hRegFuncs.end(); it++)
		{
			g_pF->fnDestroy(*it);
		}

		m_hRegFuncs.clear();

		return S_OK;
	}

	HRESULT CPluginBase::cleanupEvents()
	{
		for (std::deque<HANDLE>::iterator it = m_hHookedEvs.begin(); it != m_hHookedEvs.end(); it++)
		{
			g_pF->evUnhook(*it);
		}

		m_hHookedEvs.clear();

		return S_OK;
	}

};