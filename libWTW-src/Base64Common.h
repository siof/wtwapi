#pragma once

typedef enum
{
	step_A, step_B, step_C
} base64_encodestep;

typedef enum
{
	step_a, step_b, step_c, step_d
} base64_decodestep;

typedef struct
{
	base64_encodestep step;
	int result;
	int stepcount;
} base64_encodestate;

typedef struct
{
	base64_decodestep step;
	int plainchar;
} base64_decodestate;

int base64_decode_value(int value_in);
void base64_init_decodestate(base64_decodestate* state_in);
void base64_init_encodestate(base64_encodestate* state_in);
int base64_encode_value(int value_in);