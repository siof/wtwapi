/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/String.h>
#include <shlwapi.h>

namespace wtw
{
	CString::CString(void)
	{
	}

	CString::CString(const CString &s)
	{
		replaceBuffer((BYTE*)s.c_str(), s.getLength() * sizeof(wchar_t));
	}

	CString::CString(const wchar_t* s)
	{
		if (!s)
			return;

		replaceBuffer((BYTE*)s, wcslen(s) * sizeof(wchar_t));
	}

	CString::~CString(void)
	{
	}

	wchar_t * CString::detach()
	{
		int l;
		return (wchar_t*)CBuffer::detach(l, NULL);
	}

	void CString::clear()
	{
		disposeBuffers();
	}

	DWORD CString::getLength() const
	{
		if (getBuffer())
			return wcslen(c_str());
		else
			return 0;
	}

	bool	CString::compare(const wchar_t *str)
	{
		return (wcscmp(c_str(), str) == 0) ? true : false;
	}

	bool	CString::compareNoCase(const wchar_t *str)
	{
		return (_wcsicmp(c_str(), str) == 0) ? true : false;
	}

	const wchar_t * CString::c_str() const
	{
		return (const wchar_t*)getBuffer();
	}

	void CString::makeLower()
	{
		int l = getLength();
		wchar_t *b = (wchar_t*)getBuffer();
		for (int i = 0; i < l; ++i)
		{
			if (iswalpha(b[i]))
			{
				b[i] = towlower(b[i]);
			}
		}
	}

	void CString::makeUpper()
	{
		int l = getLength();
		wchar_t *b = (wchar_t*)getBuffer();
		for (int i = 0; i < l; ++i)
		{
			if (iswalpha(b[i]))
			{
				b[i] = towupper(b[i]);
			}
		}
	}

	CString::operator const wchar_t*() const
	{
		return (const wchar_t*)getBuffer();
	}

	CString::operator WTW_PARAM () const
	{ 
		return (WTW_PARAM)c_str(); 
	}

	CString & CString::operator += (wchar_t* data)
	{
		append(data);
		return *this;
	}

	CString & CString::operator += (const wchar_t* data)
	{
		append((wchar_t*)data);
		return *this;
	}

	CString & CString::operator += (const wchar_t data)
	{
		append((wchar_t*)&data, 2);
		return *this;
	}

	bool CString::operator == (const CString &r)
	{
		return compare(r.c_str());
	}

	bool CString::operator == (const wchar_t *r)
	{
		return compare(r);
	}

	CString & CString::operator = (const wchar_t* data)
	{
		replaceBuffer((BYTE*)data, wcslen(data) * sizeof(wchar_t));
		return *this;
	}

	CString & CString::operator = (const CString &data)
	{
		*this = (const wchar_t*)data;
		return *this;
	}

	int		CString::append(wchar_t *pData, DWORD sizeInChars)
	{
		return CBuffer::push((BYTE*)pData, sizeInChars * sizeof(wchar_t));
	}

	int		CString::append(wchar_t *pData)
	{
		return CBuffer::push((BYTE*)pData, wcslen(pData) * sizeof(wchar_t));
	}

	int		CString::format(const wchar_t *fmt, ...)
	{
		va_list v;
		va_start(v, fmt);
		int q = vswprintf(NULL, 0, fmt, v);
		resize(q * sizeof(wchar_t));
		vswprintf((wchar_t*)getBuffer(), q + 1, fmt, v);
		va_end(v);
		return q;
	}
};