/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include <wchar.h>
#include "libWTW.h"
#include <cpp/ArchiveImport.h>
#include <wtwArchive.h>
#include <Commctrl.h>

namespace wtw
{
	namespace arch
	{
		int bInTrans;

		WTW_PTR transactionBegin()
		{
			if  (bInTrans)
				return E_ABORT;

			WTW_PTR ret = g_pF->fnCall(
				WTW_ARCH_TRANSACTION, WATR_BEGIN, 0);
			bInTrans = 1;
			return ret;
		}

		WTW_PTR transactionCommit()
		{
			if  (!bInTrans)
				return E_ABORT;

			WTW_PTR ret = g_pF->fnCall(
				WTW_ARCH_TRANSACTION, WATR_COMMIT, 0);
			bInTrans = 0;
			return ret;
		}

		WTW_PTR transactionRollback()
		{
			if  (!bInTrans)
				return E_ABORT;

			WTW_PTR ret = g_pF->fnCall(
				WTW_ARCH_TRANSACTION, WATR_ROLLBACK, 0);
			bInTrans = 0;
			return ret;
		}


		WTW_PTR writeSMS(const wchar_t * phone,
			const wchar_t * text, int bIncoming, QWORD time, const wchar_t * extra)
		{
			wtwArchiveEntrySms sms;

			sms.body	= text;
			sms.extra	= extra;
			sms.phone	= phone;
			sms.time	= time;

			return g_pF->fnCall(
				WTW_ARCH_WRITE_SMS, sms, NULL);
		}

		WTW_PTR writeChatMsg(wtwMessageDef *pMessage,
			int & chatId, DWORD writeFlags)
		{
			wtwArchiveEntryMessage msg;

			msg.flags	= writeFlags;
			msg.message	= *pMessage;
			msg.chatID	= chatId;

			msg.message.msgFlags |= WTW_MESSAGE_FLAG_CHAT_MSG;

			WTW_PTR ret = g_pF->fnCall(
				WTW_ARCH_WRITE_MESSAGE, msg, NULL);
			chatId = msg.chatID;
			return ret;
		}

		WTW_PTR writeMsg(wtwMessageDef *pMessage,
			DWORD writeFlags)
		{
			wtwArchiveEntryMessage msg;

			msg.flags	= writeFlags;
			msg.message	= *pMessage;
			msg.chatID	= -1;

			msg.message.msgFlags &= ~WTW_MESSAGE_FLAG_CHAT_MSG;

			return g_pF->fnCall(
				WTW_ARCH_WRITE_MESSAGE, msg, NULL);
		}
	};

	CArchiveImport::CArchiveImport(const wchar_t *id, const wchar_t * mechName, void *cbData) : 
		m_hStatus(NULL), m_hProgress(NULL)
	{
		m_cbData		= cbData;
		m_id			= id;
		m_mechName		= mechName;
	};

	WTW_PTR CArchiveImport::_onImport(WTW_PARAM wP, WTW_PARAM lP, void *cbData)
	{
		CArchiveImport *pClass = (CArchiveImport*)cbData;
		wtwImportMechEvent *pEvent = (wtwImportMechEvent*)wP;

		pClass->m_hStatus	= pEvent->hStatus;
		pClass->m_hProgress	= pEvent->hProgess;

		::SendMessage(pClass->m_hProgress, PBM_SETRANGE32, 0, 100);

		return pClass->onImport(pClass->m_cbData, pEvent->hMainDlg);
	}

	void CArchiveImport::setProgress(int percent)
	{
		::SendMessage(m_hProgress, PBM_SETPOS, percent, 0);
	}

	void CArchiveImport::setStatusText(const wchar_t* text)
	{
		::SetWindowText(m_hStatus, text);
	}

	WTW_PTR CArchiveImport::registerMechanism()
	{
		wtwImportMech mi;

		m_cbData		= m_cbData;
		mi.mechName		= m_mechName;
		mi.callback		= &CArchiveImport::_onImport;
		mi.id			= m_id;
		mi.cbData		= this;

		return g_pF->fnCall(
			WTW_ARCH_IMPORT_REGISTER, mi, NULL);
	};

	WTW_PTR CArchiveImport::unregisterMechanism()
	{
		return g_pF->fnCall(
			WTW_ARCH_IMPORT_UNREGISTER, m_id, NULL);
	};

	CArchiveImport::~CArchiveImport()
	{
		unregisterMechanism();
	};
};