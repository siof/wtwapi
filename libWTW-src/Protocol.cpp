/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/Protocol.h>

namespace wtw 
{
	CProtocol::CProtocol(CCore *pF, const wchar_t * netClass, const wchar_t * netName) :
				m_netClass(netClass), m_netName(netName), m_conStatus(0), m_protoSID(0)
	{
		m_pF = pF;
	}

	CProtocol::~CProtocol()
	{
	}

	WTW_PTR CProtocol::_onDeleteContact(WTW_PARAM wP, WTW_PARAM lP, void *d)
	{
		ASSERT(d);
		ASSERT(wP);
		CProtocol *pClass = reinterpret_cast<CProtocol*>(d);
		wtwContactDef *pContact = reinterpret_cast<wtwContactDef*>(wP);
		return pClass->onDeleteContact(*pContact);
	}

	WTW_PTR CProtocol::_onAuthContact(WTW_PARAM wP, WTW_PARAM lP, void *d)
	{
		ASSERT(d);
		ASSERT(wP);
		CProtocol *pClass = reinterpret_cast<CProtocol*>(d);
		wtwContactDef *pContact = reinterpret_cast<wtwContactDef*>(wP);
		return pClass->onAuthContact(*pContact, lP);

	}

	WTW_PTR CProtocol::_onMoveContact(WTW_PARAM wP, WTW_PARAM lP, void *d)
	{
		ASSERT(d);
		ASSERT(wP);
		CProtocol *pClass = reinterpret_cast<CProtocol*>(d);
		wtwContactDef *pContact = reinterpret_cast<wtwContactDef*>(wP);
		wtwMoveContactDef *pGroupInfo = reinterpret_cast<wtwMoveContactDef*>(wP);
		return pClass->onMoveContact(*pContact, *pGroupInfo);
	}

	WTW_PTR CProtocol::onDeleteContact(const wtwContactDef & ct)
	{
		return E_NOTIMPL;
	}

	WTW_PTR CProtocol::onAuthContact(const wtwContactDef & ct, DWORD flags)
	{
		return E_NOTIMPL;
	}

	WTW_PTR CProtocol::onMoveContact(const wtwContactDef & ct, const wtwMoveContactDef & groupInfo)
	{
		return E_NOTIMPL;
	}

	HRESULT CProtocol::protocolRegister(const wchar_t * netDescription, wchar_t * pNetGuid, DWORD netCaps1,
				DWORD netCaps2, DWORD flags)
	{
		wtwProtocolDef protoDef;

		protoDef.protoCaps1 = netCaps1;
		protoDef.protoCaps2 = netCaps2;
		protoDef.protoDescr = netDescription;
		protoDef.netClass = m_netClass;
		protoDef.protoName = m_netName;
		protoDef.netGUID = pNetGuid;
		protoDef.flags = flags;
		protoDef.pInterface.cbData = this;
		protoDef.pInterface.pfDeleteContact = &CProtocol::_onDeleteContact;
		protoDef.pInterface.pfAuthContact = &CProtocol::_onAuthContact;
		protoDef.pInterface.pfMoveContact = &CProtocol::_onMoveContact;
		protoDef.protoState = &m_conStatus;

		WTW_PTR ret = m_pF->fnCall(
			WTW_PROTO_FUNC_ADD, protoDef, NULL);

		if (SUCCEEDED(ret))
		{
			m_protoSID = protoDef.netId;
			m_netGuid = protoDef.netGUID;
		}

		return ret;
	}

	HRESULT CProtocol::protocolUnregister()
	{
		return m_pF->fnCall(
			WTW_PROTO_FUNC_DEL, m_netClass, m_protoSID);
	}

	const wchar_t * CProtocol::getNetGuid()
	{
		return m_netGuid;
	}
};