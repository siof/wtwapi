/*
** Copyright (C) 2007-2014, K2T.eu
*/

#include "stdafx.h"
#include "libWTW.h"
#include <wchar.h>
#include <cpp/I18N.h>
#include <cpp/Globals.h>

#pragma warning (disable: 4244) // possible loss of data

namespace wtw {
	
	CTranslation::CTranslation()
	{
		_msg = g_pF->fnGet(WTW_I18N_GET_TEXT, &m_pCbData);
	}

	CTranslation::~CTranslation()
	{
	}

	int CTranslation::messageBox(
		const wchar_t * mod, DWORD id, 
		const wchar_t * defaultText, const wchar_t * defaultTitle, int defaultRet, 
		DWORD uType, HWND hParent, ...)
	{
		va_list a;
		va_start(a, hParent);
		wtwI18NMessageBox box;

		box.module		= mod;
		box.msgId		= id;
		box.defaultText	= defaultText;
		box.defaultTitle= defaultTitle;
		box.defaultRet	= defaultRet;
		box.va_list		= a;
		box.uType		= uType;
		box.hParent		= hParent;

		g_pF->fnCall(
			WTW_I18N_MESSAGE_BOX, box, NULL);

		return box.retValue;
	}

	int CTranslation::isLangAvailable(const wchar_t *lngCode)
	{
		ASSERT(g_pF);
		HRESULT hr = g_pF->fnCall(WTW_I18N_IS_LNG_AVAILABLE, (WTW_PARAM)&lngCode, NULL);
		if (SUCCEEDED(hr) && hr == S_OK)
			return 1;
		else 
			return 0;
	}

	const wchar_t * CTranslation::getCurrentLngCode()
	{
		ASSERT(g_pF);
		const wchar_t *ret;
		if (S_OK == g_pF->fnCall(WTW_I18N_GET_CURRENT_LNG_EX, (WTW_PARAM)&ret, NULL))
			return ret;
		else
			return NULL;
	}

	HRESULT CTranslation::translateHwnd(const wchar_t * mod, DWORD id, HWND hWnd)
	{
		ASSERT(g_pF);
		wtwI18NTranslateWnd m;
		initStruct(m);

		m.hWnd		= hWnd;
		m.module	= mod;
		m.windowId	= id;

		return g_pF->fnCall(WTW_I18N_TRANSLATE_WND, (WTW_PARAM)&m, NULL);
	}

	HRESULT CTranslation::dumpWindowToXml(const wchar_t * fileFullPath, HWND hWnd)
	{
		ASSERT(g_pF);
		wtwI18NDumpWindowXml w;
		initStruct(w);

		w.filePath	= fileFullPath;
		w.hWnd		= hWnd;

		return g_pF->fnCall(WTW_I18N_DUMP_WND_XML, (WTW_PARAM)&w, NULL);
	}

	const wchar_t * CTranslation::msg(const wchar_t * mod, DWORD id, const wchar_t *defaultText)
	{
		ASSERT(g_pF);
		ASSERT(_msg);

		wtwI18NGetText txt;
		initStruct(txt);

		txt.module = mod;
		txt.textId = id;
		txt.defaultText = defaultText;

		HRESULT hr;
		if (_msg)
		{
			hr = (_msg)((WTW_PARAM)&txt, NULL, m_pCbData);
		}
		else
		{
			hr = g_pF->fnCall(WTW_I18N_GET_TEXT, (WTW_PARAM)&txt, NULL);
		}

		if (SUCCEEDED(hr) && txt.text)
		{
			return txt.text;
		}

		return defaultText;
	}
};