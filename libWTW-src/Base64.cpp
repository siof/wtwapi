#include "stdafx.h"
#include <cpp/Base64.h>
#include "Base64BaseA.h"
#include "Base64BaseW.h"
#include <math.h>

#pragma warning(disable: 4244)	// possible loss of data
#pragma warning(disable: 4267)  // possible loss of data

int wtw::CBase64::encode(const BYTE *in, size_t inLen, char **out)
{
	if (!inLen || !in)
	{
		*out = NULL;
		return 0;
	}

	int needLen = ceil(inLen / 3.f) * 4.f, retLen = 0;
	*out = new char[needLen + 8]();

	base64_encodestate state;
	base64_init_encodestate(&state);
	
	retLen += base64_encode_blockA((const char*)in, inLen, *out, &state);
	retLen += base64_encode_blockendA(*out + retLen, &state);

	return retLen;
}

int wtw::CBase64::encode(const BYTE *in, size_t inLen, wchar_t **out)
{
	if (!inLen || !in)
	{
		*out = NULL;
		return 0;
	}

	int needLen = ceil(inLen / 3.f) * 4.f, retLen = 0;
	*out = new wchar_t[needLen + 8]();

	base64_encodestate state;
	base64_init_encodestate(&state);
	
	retLen += base64_encode_blockW((const char*)in, inLen, *out, &state);
	retLen += base64_encode_blockendW(*out + retLen, &state);

	return retLen;
}

int wtw::CBase64::decode(const char *in, size_t inLen, BYTE **out)
{
	if (inLen < 1 || !in)
	{
		*out = NULL;
		return 0;
	}

	int retLen = 0;
	base64_decodestate state;
	
	*out = new BYTE[inLen]();
	base64_init_decodestate(&state);
	retLen = base64_decode_blockA(in, inLen, (char*)*out, &state);

	return retLen;
}

int wtw::CBase64::decode(const wchar_t *in, size_t inLen, BYTE **out)
{
	if (inLen < 1 || !in)
	{
		*out = NULL;
		return 0;
	}

	int retLen = 0;
	base64_decodestate state;
	
	*out = new BYTE[inLen]();
	base64_init_decodestate(&state);
	retLen = base64_decode_blockW(in, inLen, (char*)*out, &state);
	return retLen;
}

void wtw::CBase64::release(wchar_t *r)
{
	delete [] r;
}

void wtw::CBase64::release(char *r)
{
	delete [] r;
}

void wtw::CBase64::release(BYTE *r)
{
	delete [] r;
}
