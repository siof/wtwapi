#pragma once

namespace wtw
{
	class LIBWTW_API CBaseObject abstract
	{
	public:
		virtual void release();
	};
};